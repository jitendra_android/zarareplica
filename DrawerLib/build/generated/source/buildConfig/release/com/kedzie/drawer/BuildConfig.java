/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.kedzie.drawer;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.kedzie.drawer";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1000000;
  public static final String VERSION_NAME = "";
}
