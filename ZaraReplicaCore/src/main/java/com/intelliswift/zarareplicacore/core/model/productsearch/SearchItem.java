package com.intelliswift.zarareplicacore.core.model.productsearch;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchItem {

    @SerializedName("sections_id")
    @Expose
    private Integer sectionsId;
    @SerializedName("section_name")
    @Expose
    private String sectionName;
    @SerializedName("total_items")
    @Expose
    private Integer totalItems;

    /**
     *
     * @return
     * The sectionsId
     */
    public Integer getSectionsId() {
        return sectionsId;
    }

    /**
     *
     * @param sectionsId
     * The sections_id
     */
    public void setSectionsId(Integer sectionsId) {
        this.sectionsId = sectionsId;
    }

    /**
     *
     * @return
     * The sectionName
     */
    public String getSectionName() {
        return sectionName;
    }

    /**
     *
     * @param sectionName
     * The section_name
     */
    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    /**
     *
     * @return
     * The totalItems
     */
    public Integer getTotalItems() {
        return totalItems;
    }

    /**
     *
     * @param totalItems
     * The total_items
     */
    public void setTotalItems(Integer totalItems) {
        this.totalItems = totalItems;
    }

}