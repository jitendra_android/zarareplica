package com.intelliswift.zarareplicacore.core.model.section;

/**
 * Created by Jitendra on 11/7/2014.
 */
public class Subcategory {

    private String sub_category_id;
    private String sub_category_name;
    private String section_id;
    private boolean image;
    private boolean has_sub_sub_category;

    public String getSub_category_name() {
        return sub_category_name;
    }

    public void setSub_category_name(String sub_category_name) {
        this.sub_category_name = sub_category_name;
    }

    public String getSub_category_id() {
        return sub_category_id;
    }

    public void setSub_category_id(String sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    public String getSection_id() {
        return section_id;
    }

    public void setSection_id(String section_id) {
        this.section_id = section_id;
    }

    public boolean isImage() {
        return image;
    }

    public void setImage(boolean image) {
        this.image = image;
    }

    public boolean isHas_sub_sub_category() {
        return has_sub_sub_category;
    }

    public void setHas_sub_sub_category(boolean has_sub_sub_category) {
        this.has_sub_sub_category = has_sub_sub_category;
    }



    @Override
    public String toString() {
        return "Subcategory{" +
                "subcategory_id='" + sub_category_id + '\'' +
                ", subcategory_name='" + sub_category_name + '\'' +
                '}';
    }
}