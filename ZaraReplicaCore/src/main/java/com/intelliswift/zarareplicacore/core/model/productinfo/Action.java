package com.intelliswift.zarareplicacore.core.model.productinfo;

/**
 * Created by omprakash on 19/11/14.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Action {

    @SerializedName("action_id")
    @Expose
    private String actionId;
    @SerializedName("action_name")
    @Expose
    private String actionName;

    /**
     *
     * @return
     * The actionId
     */
    public String getActionId() {
        return actionId;
    }

    /**
     *
     * @param actionId
     * The action_id
     */
    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    /**
     *
     * @return
     * The actionName
     */
    public String getActionName() {
        return actionName;
    }

    /**
     *
     * @param actionName
     * The action_name
     */
    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

}