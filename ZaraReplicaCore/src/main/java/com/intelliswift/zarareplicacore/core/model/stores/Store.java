package com.intelliswift.zarareplicacore.core.model.stores;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Store implements Parcelable {

    private String city;
    private String address;
    private String name;
    private String details;
    private String lattitude;
    private String longitude;
    private String todaystiming;
    private List<String> contactnumber = new ArrayList<String>();

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getLattitude() {
        return lattitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTodaystiming() {
        return todaystiming;
    }

    public void setTodaystiming(String todaystiming) {
        this.todaystiming = todaystiming;
    }

    public List<String> getContactnumber() {
        return contactnumber;
    }

    public void setContactnumber(List<String> contactnumber) {
        this.contactnumber = contactnumber;
    }

    protected Store(Parcel in) {
        city = in.readString();
        address = in.readString();
        name = in.readString();
        details = in.readString();
        lattitude = in.readString();
        longitude = in.readString();
        todaystiming = in.readString();
        if (in.readByte() == 0x01) {
            contactnumber = new ArrayList<String>();
            in.readList(contactnumber, String.class.getClassLoader());
        } else {
            contactnumber = null;
        }
    }

    @Override
    public String toString() {
        return "Store{" +
                "city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", name='" + name + '\'' +
                ", details='" + details + '\'' +
                ", lattitude='" + lattitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", todaystiming='" + todaystiming + '\'' +
                ", contactnumber=" + contactnumber +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(city);
        dest.writeString(address);
        dest.writeString(name);
        dest.writeString(details);
        dest.writeString(lattitude);
        dest.writeString(longitude);
        dest.writeString(todaystiming);
        if (contactnumber == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(contactnumber);
        }
    }

    @SuppressWarnings("unused")
    public static final Creator<Store> CREATOR = new Creator<Store>() {
        @Override
        public Store createFromParcel(Parcel in) {
            return new Store(in);
        }

        @Override
        public Store[] newArray(int size) {
            return new Store[size];
        }
    };


}


