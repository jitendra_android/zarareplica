package com.intelliswift.zarareplicacore.core.model.productlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Product {

    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_prize")
    @Expose
    private String productPrize;
    @SerializedName("product_url")
    @Expose
    private String productUrl;
    @SerializedName("sections_id")
    @Expose
    private Integer sectionsId;
    @SerializedName("product_catid")
    @Expose
    private List<String> productCatid = new ArrayList<String>();

    /**
     *
     * @return
     * The productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     *
     * @param productName
     * The product_name
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     *
     * @return
     * The productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     *
     * @param productId
     * The product_id
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     *
     * @return
     * The productPrize
     */
    public String getProductPrize() {
        return productPrize;
    }

    /**
     *
     * @param productPrize
     * The product_prize
     */
    public void setProductPrize(String productPrize) {
        this.productPrize = productPrize;
    }

    /**
     *
     * @return
     * The productUrl
     */
    public String getProductUrl() {
        return productUrl;
    }

    /**
     *
     * @param productUrl
     * The product_url
     */
    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    /**
     *
     * @return
     * The sectionsId
     */
    public Integer getSectionsId() {
        return sectionsId;
    }

    /**
     *
     * @param sectionsId
     * The sections_id
     */
    public void setSectionsId(Integer sectionsId) {
        this.sectionsId = sectionsId;
    }

    /**
     *
     * @return
     * The productCatid
     */
    public List<String> getProductCatid() {
        return productCatid;
    }

    /**
     *
     * @param productCatid
     * The product_catid
     */
    public void setProductCatid(List<String> productCatid) {
        this.productCatid = productCatid;
    }

}