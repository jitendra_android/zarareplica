package com.intelliswift.zarareplicacore.core;

/**
 * Created by Jitendra on 11/12/2014.
 */
public interface IBaseFacade {
    /*
    * Interface used for having common functionalites in all facades
    * */
   public void setTag(String tag);
    /*
    Method for setting the tag for each facade to recognize the request generator.
     */
}
