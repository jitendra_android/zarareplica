package com.intelliswift.zarareplicacore.core.stores;

import com.intelliswift.zarareplicacore.core.IBaseFacade;

/**
 * Created by Swapnil on 12/9/2014.
 */
public interface IStoresFacade extends IBaseFacade {

    public void getNearbyStores();
    public void searchStores();
//    public void getStoreDetails();


}
