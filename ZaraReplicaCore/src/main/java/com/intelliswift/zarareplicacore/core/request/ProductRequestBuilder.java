package com.intelliswift.zarareplicacore.core.request;

import com.intelliswift.zarareplicacore.core.model.productinfo.ProductInfoResponse;
import com.intelliswift.zarareplicacore.core.model.productlist.ProductListResponse;
import com.intelliswift.zarareplicacore.core.model.productsearch.ProductSerachResponse;
import com.intelliswift.zarareplicacore.core.model.section.SectionResponse;

import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Jitendra on 11/12/2014.
 */
public class ProductRequestBuilder extends RequestBuilder {

    public interface ProductService {

        @GET("/catapi/cat")
        void getSections(ReturnCallback<SectionResponse> sectionResponseReturnCallback);

        @GET("/productinfoapi/{productId}")
        void getProductInfo(@Path("productId") String productId, ReturnCallback<ProductInfoResponse> sectionResponseReturnCallback);

        @GET("/productlistapi/list")
        void getProductList(ReturnCallback<ProductListResponse> productListResponseReturnCallback);

        @GET("/productsearchapi/{item}")
        void getSearchProductResult(@Path("item") String item,ReturnCallback<ProductSerachResponse> productSearchReturnCallback);

    }

    public ProductService getService() {
        return super.build().create(ProductService.class);
    }
}
