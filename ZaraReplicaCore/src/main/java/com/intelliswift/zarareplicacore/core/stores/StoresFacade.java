package com.intelliswift.zarareplicacore.core.stores;

import com.intelliswift.zarareplicacore.core.IResponseSubscribe;
import com.intelliswift.zarareplicacore.core.application.IApplicationFacade;
import com.intelliswift.zarareplicacore.core.model.stores.StoreResponse;
import com.intelliswift.zarareplicacore.core.request.StoresRequestBuilder;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by Swapnil on 12/10/2014.
 */
public class StoresFacade implements IStoresFacade, IResponseSubscribe {

    private String TAG;
    private IResponseSubscribe responseObserver;
    private IApplicationFacade applicationFacade;

    private StoresRequestBuilder builder;

    public StoresFacade(IResponseSubscribe responseObserver, IApplicationFacade applicationFacade) {
        this.responseObserver = responseObserver;
        this.applicationFacade = applicationFacade;
        this.builder = new StoresRequestBuilder();
    }

    @Override
    public void setTag(String tag) {
        this.TAG = tag;
    }

    @Override
    public void onResponse(Object response, String tag) {
        responseObserver.onResponse(response, tag);
    }

    @Override
    public void onErrorResponse(Exception error) {
        responseObserver.onErrorResponse(error);
    }

    @Override
    public void getNearbyStores() {
        builder.getService().getStoresList(new Callback<StoreResponse>() {
            @Override
            public void success(StoreResponse storeResponse, Response response) {
                onResponse(storeResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error);
            }
        });
    }

    @Override
    public void searchStores() {
        builder.getService().getStoresList(new Callback<StoreResponse>() {
            @Override
            public void success(StoreResponse storeResponse, Response response) {
                onResponse(storeResponse, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error);
            }
        });
    }


}
