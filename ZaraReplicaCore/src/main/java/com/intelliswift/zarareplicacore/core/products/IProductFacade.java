package com.intelliswift.zarareplicacore.core.products;

import com.intelliswift.zarareplicacore.core.IBaseFacade;

/**
 * Created by Jitendra on 11/12/2014.
 */
public interface IProductFacade extends IBaseFacade{
    public void getProductSectionList();
    public  void getProductList();
    public void getProductInfo(String productId);
    public void getSearchProductResult(String item);

}
