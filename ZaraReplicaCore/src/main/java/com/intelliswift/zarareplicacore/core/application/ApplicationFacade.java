package com.intelliswift.zarareplicacore.core.application;

/**
 * Created by Jitendra on 11/12/2014.
 */
public class ApplicationFacade implements IApplicationFacade {
    private String TAG;

    private ApplicationFacade() {

    }

    @Override
    public void setTag(String tag) {
        TAG = tag;
    }
}
