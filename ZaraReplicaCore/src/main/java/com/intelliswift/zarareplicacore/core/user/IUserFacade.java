package com.intelliswift.zarareplicacore.core.user;

import com.intelliswift.zarareplicacore.core.IBaseFacade;

/**
 * Created by Swapnil on 2/18/2015.
 */
public interface IUserFacade extends IBaseFacade {

    public void getUserProfileInfo(String userId);
}
