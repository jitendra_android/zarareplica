package com.intelliswift.zarareplicacore.core.model.productinfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by omprakash on 19/11/14.
 */
public class ProductInfoResponse {

    @Expose
    private String errorcode;
    @Expose
    private String errormessage;
    @SerializedName("products_info")
    @Expose
    private ProductsInfo productsInfo;

    /**
     *
     * @return
     * The errorcode
     */
    public String getErrorcode() {
        return errorcode;
    }

    /**
     *
     * @param errorcode
     * The errorcode
     */
    public void setErrorcode(String errorcode) {
        this.errorcode = errorcode;
    }

    /**
     *
     * @return
     * The errormessage
     */
    public String getErrormessage() {
        return errormessage;
    }

    /**
     *
     * @param errormessage
     * The errormessage
     */
    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    /**
     *
     * @return
     * The productsInfo
     */
    public ProductsInfo getProductsInfo() {
        return productsInfo;
    }

    /**
     *
     * @param productsInfo
     * The products_info
     */
    public void setProductsInfo(ProductsInfo productsInfo) {
        this.productsInfo = productsInfo;
    }
}
