package com.intelliswift.zarareplicacore.core;

public interface IResponseSubscribe {

    public void onResponse(Object response, String tag);

    public void onErrorResponse(Exception error);

}
