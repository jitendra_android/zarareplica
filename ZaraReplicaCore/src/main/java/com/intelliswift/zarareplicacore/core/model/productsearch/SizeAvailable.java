package com.intelliswift.zarareplicacore.core.model.productsearch;

import com.google.gson.annotations.Expose;

/**
 * Created by Rohan on 12/12/2014.
 */
public class SizeAvailable {

    @Expose
    private String size;

    /**
     * @return The size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size The size
     */
    public void setSize(String size) {
        this.size = size;
    }

}
