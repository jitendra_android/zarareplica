package com.intelliswift.zarareplicacore.core.model.productinfo;

/**
 * Created by omprakash on 19/11/14.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Info {
    @Expose
    private String description;
    @SerializedName("composition_care")
    @Expose
    private List<Object> compositionCare = new ArrayList<Object>();
    @Expose
    private Details details;
    @Expose
    private List<String> pid = new ArrayList<String>();
    @SerializedName("instore_availability")
    @Expose
    private List<InstoreAvailability> instoreAvailability = new ArrayList<InstoreAvailability>();

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The compositionCare
     */
    public List<Object> getCompositionCare() {
        return compositionCare;
    }

    /**
     *
     * @param compositionCare
     * The composition_care
     */
    public void setCompositionCare(List<Object> compositionCare) {
        this.compositionCare = compositionCare;
    }

    /**
     *
     * @return
     * The details
     */
    public Details getDetails() {
        return details;
    }

    /**
     *
     * @param details
     * The details
     */
    public void setDetails(Details details) {
        this.details = details;
    }

    /**
     *
     * @return
     * The pid
     */
    public List<String> getPid() {
        return pid;
    }

    /**
     *
     * @param pid
     * The pid
     */
    public void setPid(List<String> pid) {
        this.pid = pid;
    }

    /**
     *
     * @return
     * The instoreAvailability
     */
    public List<InstoreAvailability> getInstoreAvailability() {
        return instoreAvailability;
    }

    /**
     *
     * @param instoreAvailability
     * The instore_availability
     */
    public void setInstoreAvailability(List<InstoreAvailability> instoreAvailability) {
        this.instoreAvailability = instoreAvailability;
    }
}