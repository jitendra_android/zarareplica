package com.intelliswift.zarareplicacore.core.model.user;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileInfoResponse {

    @Expose
    private String errorcode;
    @Expose
    private String errormessage;
    @SerializedName("personal_details")
    @Expose
    private List<PersonalDetail> personalDetails = new ArrayList<PersonalDetail>();

    /**
     * @return The errorcode
     */
    public String getErrorcode() {
        return errorcode;
    }

    /**
     * @param errorcode The errorcode
     */
    public void setErrorcode(String errorcode) {
        this.errorcode = errorcode;
    }

    /**
     * @return The errormessage
     */
    public String getErrormessage() {
        return errormessage;
    }

    /**
     * @param errormessage The errormessage
     */
    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    /**
     * @return The personalDetails
     */
    public List<PersonalDetail> getPersonalDetails() {
        return personalDetails;
    }

    /**
     * @param personalDetails The personal_details
     */
    public void setPersonalDetails(List<PersonalDetail> personalDetails) {
        this.personalDetails = personalDetails;
    }

}