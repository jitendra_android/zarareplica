package com.intelliswift.zarareplicacore.core.model.productsearch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.intelliswift.zarareplicacore.core.model.productlist.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductSerachResponse {

    @Expose
    private String errorcode;
    @Expose
    private String errormessage;
    @SerializedName("search_item")
    @Expose
    private List<SearchItem> searchItem = new ArrayList<SearchItem>();
    @Expose
    private List<Product> products = new ArrayList<Product>();

    /**
     *
     * @return
     * The errorcode
     */
    public String getErrorcode() {
        return errorcode;
    }

    /**
     *
     * @param errorcode
     * The errorcode
     */
    public void setErrorcode(String errorcode) {
        this.errorcode = errorcode;
    }

    /**
     *
     * @return
     * The errormessage
     */
    public String getErrormessage() {
        return errormessage;
    }

    /**
     *
     * @param errormessage
     * The errormessage
     */
    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    /**
     *
     * @return
     * The searchItem
     */
    public List<SearchItem> getSearchItem() {
        return searchItem;
    }

    /**
     *
     * @param searchItem
     * The search_item
     */
    public void setSearchItem(List<SearchItem> searchItem) {
        this.searchItem = searchItem;
    }

    /**
     *
     * @return
     * The products
     */
    public List<Product> getProducts() {
        return products;
    }

    /**
     *
     * @param products
     * The products
     */
    public void setProducts(List<Product> products) {
        this.products = products;
    }

}