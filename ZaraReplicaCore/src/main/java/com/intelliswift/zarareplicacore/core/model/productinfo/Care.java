package com.intelliswift.zarareplicacore.core.model.productinfo;

/**
 * Created by omprakash on 19/11/14.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Care {

    @SerializedName("care_id")
    @Expose
    private String careId;
    @SerializedName("care_type")
    @Expose
    private String careType;

    /**
     *
     * @return
     * The careId
     */
    public String getCareId() {
        return careId;
    }

    /**
     *
     * @param careId
     * The care_id
     */
    public void setCareId(String careId) {
        this.careId = careId;
    }

    /**
     *
     * @return
     * The careType
     */
    public String getCareType() {
        return careType;
    }

    /**
     *
     * @param careType
     * The care_type
     */
    public void setCareType(String careType) {
        this.careType = careType;
    }

}