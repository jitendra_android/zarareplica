package com.intelliswift.zarareplicacore.core;

import android.content.Context;

import com.intelliswift.zarareplicacore.core.products.ProductFacade;
import com.intelliswift.zarareplicacore.core.stores.StoresFacade;
import com.intelliswift.zarareplicacore.core.user.UserFacade;

import java.util.HashMap;

/**
 * Created by Jitendra on 11/7/2014.
 */
public class IOCContainer {
    private HashMap<ObjectName, Object> objectContainer;
    public ResponsePublisher publisher;
    private static IOCContainer instance;
    private Context context;

    public Context getContext() {
        return context;
    }


    public enum ObjectName {PRODUCT_SERVICE, STORES_SERVICE,USER_SERVICE}

    private IOCContainer(Context context) {
        objectContainer = new HashMap<ObjectName, Object>();
        publisher = new ResponsePublisher();

    }

    //Applying singleton
    public static IOCContainer getInstance(Context context) {
        if (instance == null) {
            instance = new IOCContainer(context);
        }
        return instance;
    }

    //Returns specific facade object depending on the name provided only need to typecast to its required class
    public IBaseFacade getObject(ObjectName name, String tag) {
        IBaseFacade facadeObject = (IBaseFacade) objectContainer.get(name);
        if (facadeObject == null) {
            switch (name) {
                case PRODUCT_SERVICE:
                    facadeObject = new ProductFacade(publisher, null);
                    break;

                case STORES_SERVICE:
                    facadeObject = new StoresFacade(publisher, null);
                    break;

                case USER_SERVICE:
                    facadeObject = new UserFacade(publisher, null);
                    break;

            }
        }
        facadeObject.setTag(tag);
        return facadeObject;
    }

}
