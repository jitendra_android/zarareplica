package com.intelliswift.zarareplicacore.core.request;

import com.intelliswift.zarareplicacore.core.model.user.ProfileInfoResponse;

import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Swapnil on 2/18/2015.
 */
public class UserRequestBuilder extends RequestBuilder {

    public interface UserService {

        @GET("/customerinfoapi/{user_id}")
        void getUserProfileInfo(@Path("user_id")String userID, ReturnCallback<ProfileInfoResponse> sectionResponseReturnCallback);
    }

    public UserService getService() {
        return super.build().create(UserService.class);
    }
}
