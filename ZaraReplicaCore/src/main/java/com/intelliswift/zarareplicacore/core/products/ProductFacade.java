package com.intelliswift.zarareplicacore.core.products;

import android.util.Log;

import com.intelliswift.zarareplicacore.core.IResponseSubscribe;
import com.intelliswift.zarareplicacore.core.application.IApplicationFacade;
import com.intelliswift.zarareplicacore.core.model.productinfo.ProductInfoResponse;
import com.intelliswift.zarareplicacore.core.model.productlist.ProductListResponse;
import com.intelliswift.zarareplicacore.core.model.productsearch.ProductSerachResponse;
import com.intelliswift.zarareplicacore.core.model.section.SectionResponse;
import com.intelliswift.zarareplicacore.core.request.ProductRequestBuilder;
import com.intelliswift.zarareplicacore.core.request.ReturnCallback;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Jitendra on 11/12/2014.
 */
public class ProductFacade implements IProductFacade, IResponseSubscribe {

    private String TAG;
    private IResponseSubscribe responseObserver;
    private IApplicationFacade applicationFacade;

    private ProductRequestBuilder builder;

    public ProductFacade(IResponseSubscribe responseObserver, IApplicationFacade applicationFacade) {
        this.responseObserver = responseObserver;
        this.applicationFacade = applicationFacade;
        this.builder = new ProductRequestBuilder();
    }

    @Override
    public void getProductInfo(String productId) {

        builder.getService().getProductInfo(productId, new ReturnCallback<ProductInfoResponse>() {
//            @Override
//            public void success(ProductInfoResponse productInfoResponse, Response response) {
//                onResponse(productInfoResponse, TAG);
//            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error);
            }

            @Override
            public void successResponse(ProductInfoResponse o, Response response) {
                onResponse(o, TAG);
            }
        });
    }

    @Override
    public void getSearchProductResult(String item) {
        builder.getService().getSearchProductResult(item,new ReturnCallback<ProductSerachResponse>() {
            @Override
            public void successResponse(ProductSerachResponse o, Response response) {
                onResponse(o, TAG);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                onErrorResponse(retrofitError);
            }
        });
    }


    @Override
    public void getProductSectionList() {

        builder.getService().getSections(new ReturnCallback<SectionResponse>() {

//            public void (SectionResponse sectionResponse, Response response) {
//                onResponse(sectionResponse, TAG);
//            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error);
            }

            @Override
            public void successResponse(SectionResponse o, Response response) {
                Log.d("Return Object", "Call On successResponse");

                onResponse(o, TAG);
            }
        });
    }

    @Override
    public void getProductList() {
        builder.getService().getProductList(new ReturnCallback<ProductListResponse>() {
            //            @Override
//            public void success(ProductListResponse productListResponse, Response response) {
//                onResponse(productListResponse, TAG);
//            }
            @Override
            public void successResponse(ProductListResponse o, Response response) {
                Log.d("Return Object", "Call On successResponse");

                onResponse(o, TAG);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error);
            }
        });


    }

    @Override
    public void setTag(String tag) {
        TAG = tag;
    }

    @Override
    public void onResponse(Object response, String tag) {
        responseObserver.onResponse(response, tag);
    }

    @Override
    public void onErrorResponse(Exception error) {
        responseObserver.onErrorResponse(error);
    }
}
