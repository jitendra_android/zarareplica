package com.intelliswift.zarareplicacore.core.model.productinfo;

/**
 * Created by omprakash on 19/11/14.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ProductsInfo {

    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_prize")
    @Expose
    private String productPrize;
    @SerializedName("product_url")
    @Expose
    private String productUrl;
    @SerializedName("sections_id")
    @Expose
    private Integer sectionsId;
    @SerializedName("product_catid")
    @Expose
    private List<Object> productCatid = new ArrayList<Object>();
    @SerializedName("image_url")
    @Expose
    private List<String> imageUrl = new ArrayList<String>();
    @Expose
    private Info info;

    /**
     *
     * @return
     * The productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     *
     * @param productName
     * The product_name
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     *
     * @return
     * The productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     *
     * @param productId
     * The product_id
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     *
     * @return
     * The productPrize
     */
    public String getProductPrize() {
        return productPrize;
    }

    /**
     *
     * @param productPrize
     * The product_prize
     */
    public void setProductPrize(String productPrize) {
        this.productPrize = productPrize;
    }

    /**
     *
     * @return
     * The productUrl
     */
    public String getProductUrl() {
        return productUrl;
    }

    /**
     *
     * @param productUrl
     * The product_url
     */
    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    /**
     *
     * @return
     * The sectionsId
     */
    public Integer getSectionsId() {
        return sectionsId;
    }

    /**
     *
     * @param sectionsId
     * The sections_id
     */
    public void setSectionsId(Integer sectionsId) {
        this.sectionsId = sectionsId;
    }

    /**
     *
     * @return
     * The productCatid
     */
    public List<Object> getProductCatid() {
        return productCatid;
    }

    /**
     *
     * @param productCatid
     * The product_catid
     */
    public void setProductCatid(List<Object> productCatid) {
        this.productCatid = productCatid;
    }

    /**
     *
     * @return
     * The imageUrl
     */
    public List<String> getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @param imageUrl
     * The image_url
     */
    public void setImageUrl(List<String> imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     *
     * @return
     * The info
     */
    public Info getInfo() {
        return info;
    }

    /**
     *
     * @param info
     * The info
     */
    public void setInfo(Info info) {
        this.info = info;
    }
}