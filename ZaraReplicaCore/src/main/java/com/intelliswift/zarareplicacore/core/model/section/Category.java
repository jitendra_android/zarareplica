package com.intelliswift.zarareplicacore.core.model.section;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Category {

    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @Expose
    private String image;
    @SerializedName("has_sub_category")
    @Expose
    private String hasSubCategory;

    public List<Subcategory> getSub_categories() {
        return sub_categories;
    }

    public void setSub_categories(List<Subcategory> sub_categories) {
        this.sub_categories = sub_categories;
    }

    @SerializedName("sub_categories")
    @Expose
    private List<Subcategory> sub_categories = new ArrayList<Subcategory>();

    /**
     *
     * @return
     * The categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     *
     * @param categoryName
     * The category_name
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     *
     * @return
     * The categoryId
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     *
     * @param categoryId
     * The category_id
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /**
     *
     * @return
     * The image
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     *
     * @return
     * The hasSubCategory
     */
    public String getHasSubCategory() {
        return hasSubCategory;
    }

    /**
     *
     * @param hasSubCategory
     * The has_sub_category
     */
    public void setHasSubCategory(String hasSubCategory) {
        this.hasSubCategory = hasSubCategory;
    }

}