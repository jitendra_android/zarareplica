package com.intelliswift.zarareplicacore.core.model.stores;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Swapnil on 11/7/2014.
 */
public class StoreResponse implements Parcelable{

    private String errorcode;
    private String errormessage;
    private List<Store> store = new ArrayList<Store>();

    /**
     * @return The errorcode
     */
    public String getErrorcode() {
        return errorcode;
    }

    /**
     * @param errorcode The errorcode
     */
    public void setErrorcode(String errorcode) {
        this.errorcode = errorcode;
    }

    /**
     * @return The errormessage
     */
    public String getErrormessage() {
        return errormessage;
    }

    /**
     * @param errormessage The errormessage
     */
    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    /**
     * @return The store
     */
    public List<Store> getStore() {
        return store;
    }

    /**
     * @param store The store
     */
    public void setStore(List<Store> store) {
        this.store = store;
    }


    protected StoreResponse(Parcel in) {
        errorcode = in.readString();
        errormessage = in.readString();
        if (in.readByte() == 0x01) {
            store = new ArrayList<Store>();
            in.readList(store, Store.class.getClassLoader());
        } else {
            store = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(errorcode);
        dest.writeString(errormessage);
        if (store == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(store);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<StoreResponse> CREATOR = new Parcelable.Creator<StoreResponse>() {
        @Override
        public StoreResponse createFromParcel(Parcel in) {
            return new StoreResponse(in);
        }

        @Override
        public StoreResponse[] newArray(int size) {
            return new StoreResponse[size];
        }
    };


}
