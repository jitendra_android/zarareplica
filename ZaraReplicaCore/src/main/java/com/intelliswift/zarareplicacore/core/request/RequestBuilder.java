package com.intelliswift.zarareplicacore.core.request;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by Jitendra on 11/12/2014.
 */
public abstract class RequestBuilder {
    /*
    Base class of request builder with RestAdapter as required by Retrofit implementation
     */
//    public static final String BASE_URL = "http://private-6b99a-shoppedia.apiary-mock.com";
    public static String BASE_URL = "http://staging.intelliswift.co.in/sdt/magento1.9/api/rest";
    RestAdapter.Builder restBuilder;

    protected RestAdapter build() {
        /*
        Method used for building the final request.
         */

        RetrofitHttpOAuthConsumer oAuthConsumer = new RetrofitHttpOAuthConsumer("fd1d1408a9a907446e207340d4af4e9e", "fd516dab995f38d3867cd3617599b120");
        oAuthConsumer.setTokenWithSecret("e7328f51e5c9dfe18be0b30bd3f9c0dd", "3590640ee5f6b922bec29bc1f1daa333");
        OkClient client = new SigningOkClient(oAuthConsumer);

//        if (restBuilder == null) {
        restBuilder = new RestAdapter.Builder().setEndpoint(BASE_URL).setLogLevel(RestAdapter.LogLevel.FULL).setRequestInterceptor(requestInterceptor);


//        }
        return restBuilder.
                setClient(client).build();
    }

    RequestInterceptor requestInterceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            request.addHeader("Accept", "application/json");
        }
    };


}
