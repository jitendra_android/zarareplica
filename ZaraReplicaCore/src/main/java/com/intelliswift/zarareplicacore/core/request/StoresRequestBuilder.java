package com.intelliswift.zarareplicacore.core.request;

import com.intelliswift.zarareplicacore.core.model.stores.StoreResponse;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by Swapnil on 12/10/2014.
 */
public class StoresRequestBuilder extends RequestBuilder{

    public interface StoresServeice{

        @GET("/getstorelist")
        void getStoresList(Callback<StoreResponse> storesResponseCallback);

        @GET("/getSearchStore")
        void searchStores(Callback<StoreResponse> storesResponseCallback);

    }

    public StoresServeice getService() {
        return super.build().create(StoresServeice.class);
    }
}
