package com.intelliswift.zarareplicacore.core.model.section;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Section {

    @SerializedName("section_name")
    @Expose
    private String sectionName;
    @SerializedName("section_id")
    @Expose
    private String sectionId;
    @Expose
    private String image;
    @SerializedName("has_category")
    @Expose
    private String hasCategory;
    @Expose
    private List<Category> categories = new ArrayList<Category>();

    /**
     * @return The sectionName
     */
    public String getSectionName() {
        return sectionName;
    }

    /**
     * @param sectionName The section_name
     */
    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    /**
     * @return The sectionId
     */
    public String getSectionId() {
        return sectionId;
    }

    /**
     * @param sectionId The section_id
     */
    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The hasCategory
     */
    public String getHasCategory() {
        return hasCategory;
    }

    /**
     * @param hasCategory The has_category
     */
    public void setHasCategory(String hasCategory) {
        this.hasCategory = hasCategory;
    }

    /**
     * @return The categories
     */
    public List<Category> getCategories() {
        return categories;
    }

    /**
     * @param categories The categories
     */
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

}