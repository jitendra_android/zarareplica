package com.intelliswift.zarareplicacore.core.model.productinfo;

/**
 * Created by omprakash on 19/11/14.
 */

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class CompositionCare {

    @Expose
    private String description;
    @Expose
    private List<String> compositions = new ArrayList<String>();
    @Expose
    private List<Care> care = new ArrayList<Care>();

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The compositions
     */
    public List<String> getCompositions() {
        return compositions;
    }

    /**
     *
     * @param compositions
     * The compositions
     */
    public void setCompositions(List<String> compositions) {
        this.compositions = compositions;
    }

    /**
     *
     * @return
     * The care
     */
    public List<Care> getCare() {
        return care;
    }

    /**
     *
     * @param care
     * The care
     */
    public void setCare(List<Care> care) {
        this.care = care;
    }

}