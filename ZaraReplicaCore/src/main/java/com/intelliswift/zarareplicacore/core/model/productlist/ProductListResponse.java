package com.intelliswift.zarareplicacore.core.model.productlist;

import java.util.ArrayList;
import java.util.List;

public class ProductListResponse {

    private String errorcode;
    private String errormessage;
    private List<Product> products = new ArrayList<Product>();

    /**
     * @return The errorcode
     */
    public String getErrorcode() {
        return errorcode;
    }

    /**
     * @param errorcode The errorcode
     */
    public void setErrorcode(String errorcode) {
        this.errorcode = errorcode;
    }

    /**
     * @return The errormessage
     */
    public String getErrormessage() {
        return errormessage;
    }

    /**
     * @param errormessage The errormessage
     */
    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    /**
     * @return The products
     */
    public List<Product> getProducts() {
        return products;
    }

    /**
     * @param products The products
     */
    public void setProducts(List<Product> products) {
        this.products = products;
    }
}