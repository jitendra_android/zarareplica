package com.intelliswift.zarareplicacore.core.model.productinfo;

/**
 * Created by omprakash on 19/11/14.
 */
import com.google.gson.annotations.Expose;

public class InstoreAvailability {

    @Expose
    private String size;
    @Expose
    private String isAvailable;

    /**
     *
     * @return
     * The size
     */
    public String getSize() {
        return size;
    }

    /**
     *
     * @param size
     * The size
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     *
     * @return
     * The isAvailable
     */
    public String getIsAvailable() {
        return isAvailable;
    }

    /**
     *
     * @param isAvailable
     * The isAvailable
     */
    public void setIsAvailable(String isAvailable) {
        this.isAvailable = isAvailable;
    }
}