package com.intelliswift.zarareplicacore.core;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Jitendra on 11/12/2014.
 */
public class ResponsePublisher implements IResponseSubscribe {

    private CopyOnWriteArrayList<IResponseSubscribe> responseObservers;

    public ResponsePublisher() {
        responseObservers = new CopyOnWriteArrayList<IResponseSubscribe>();
    }

    public void registerResponseSubscribe(IResponseSubscribe responseObserver) {
        if (!responseObservers.contains(responseObserver)) {
            responseObservers.add(responseObserver);
        }

    }

    @Override
    public void onResponse(Object response, String tag) {
        System.err.println("ResponsePublisher.onResponse()");

        for (IResponseSubscribe observer : responseObservers) {
            observer.onResponse(response, tag);
        }
    }

    @Override
    public void onErrorResponse(Exception error) {
        System.err.println("ResponsePublisher.onErrorResponse()");

        for (IResponseSubscribe observer : responseObservers) {
            observer.onErrorResponse(error);
        }
    }

    public void unregisterResponseSubscribe(IResponseSubscribe responseObserver) {
        responseObservers.remove(responseObserver);
    }
}
