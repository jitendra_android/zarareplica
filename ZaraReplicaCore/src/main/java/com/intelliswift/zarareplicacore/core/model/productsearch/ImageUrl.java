package com.intelliswift.zarareplicacore.core.model.productsearch;

import com.google.gson.annotations.Expose;

/**
 * Created by Rohan on 12/12/2014.
 */
public class ImageUrl {

@Expose
private String url;

/**
*
* @return
* The url
*/
public String getUrl() {
return url;
}

/**
*
* @param url
* The url
*/
public void setUrl(String url) {
this.url = url;
}

}
