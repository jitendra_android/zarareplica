package com.intelliswift.zarareplicacore.core.request;

import com.google.gson.Gson;

import java.lang.reflect.ParameterizedType;

import retrofit.Callback;
import retrofit.client.Response;

/**
 * Created by Swapnil on 2/2/2015.
 */
public abstract class ReturnCallback<T> implements Callback<String> {

    @Override
    public void success(String o, Response response) {

        Gson gson = new Gson();
        T lstObject;
        Class<T> persistentClass = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
        lstObject = gson.fromJson(o, persistentClass);
        successResponse(lstObject, response);
    }

    public abstract void successResponse(T o, Response response);


}
