package com.intelliswift.zarareplicacore.core.user;

import com.intelliswift.zarareplicacore.core.IResponseSubscribe;
import com.intelliswift.zarareplicacore.core.application.IApplicationFacade;
import com.intelliswift.zarareplicacore.core.model.user.ProfileInfoResponse;
import com.intelliswift.zarareplicacore.core.request.ReturnCallback;
import com.intelliswift.zarareplicacore.core.request.UserRequestBuilder;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Swapnil on 2/18/2015.
 */
public class UserFacade implements IUserFacade,IResponseSubscribe {

    private String TAG;
    private IResponseSubscribe responseObserver;
    private IApplicationFacade applicationFacade;

    private UserRequestBuilder builder;

    public UserFacade(IResponseSubscribe responseObserver, IApplicationFacade applicationFacade) {
        this.responseObserver = responseObserver;
        this.applicationFacade = applicationFacade;
        this.builder = new UserRequestBuilder();
    }


    @Override
    public void setTag(String tag) {
        this.TAG = tag;
    }

    @Override
    public void onResponse(Object response, String tag) {
        responseObserver.onResponse(response, tag);
    }

    @Override
    public void onErrorResponse(Exception error) {

    }

    @Override
    public void getUserProfileInfo(String userId) {
        builder.getService().getUserProfileInfo(userId, new ReturnCallback<ProfileInfoResponse>() {
            @Override
            public void successResponse(ProfileInfoResponse o, Response response) {
                onResponse(o,TAG);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                onErrorResponse(retrofitError);
            }
        });
    }
}
