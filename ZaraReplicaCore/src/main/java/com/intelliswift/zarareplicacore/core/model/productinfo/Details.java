package com.intelliswift.zarareplicacore.core.model.productinfo;

/**
 * Created by omprakash on 19/11/14.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details {

    @Expose
    private String title;
    @SerializedName("refer_no")
    @Expose
    private String referNo;
    @Expose
    private String type;

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The referNo
     */
    public String getReferNo() {
        return referNo;
    }

    /**
     *
     * @param referNo
     * The refer_no
     */
    public void setReferNo(String referNo) {
        this.referNo = referNo;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

}