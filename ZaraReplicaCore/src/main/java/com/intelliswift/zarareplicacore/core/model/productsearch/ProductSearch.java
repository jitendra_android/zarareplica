package com.intelliswift.zarareplicacore.core.model.productsearch;

import com.intelliswift.zarareplicacore.core.model.productinfo.Info;
import com.intelliswift.zarareplicacore.core.model.productlist.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductSearch extends Product {

    private List<ImageUrl> image_urls = new ArrayList<ImageUrl>();
    private Info info;


    public List<ImageUrl> getImage_urls() {
        return image_urls;
    }

    /**
     * @param image_urls The image_urls
     */
    public void setImage_urls(List<ImageUrl> image_urls) {
        this.image_urls = image_urls;
    }

    /**
     * @return The info
     */
    public Info getInfo() {
        return info;
    }

    /**
     * @param info The info
     */
    public void setInfo(Info info) {
        this.info = info;
    }

}