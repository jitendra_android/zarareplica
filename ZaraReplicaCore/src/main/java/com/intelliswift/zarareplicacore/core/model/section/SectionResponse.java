package com.intelliswift.zarareplicacore.core.model.section;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class SectionResponse {

    @Expose
    private String errorcode;
    @Expose
    private String errormessage;
    @Expose
    private List<Section> sections = new ArrayList<Section>();

    /**
     *
     * @return
     * The errorcode
     */
    public String getErrorcode() {
        return errorcode;
    }

    /**
     *
     * @param errorcode
     * The errorcode
     */
    public void setErrorcode(String errorcode) {
        this.errorcode = errorcode;
    }

    /**
     *
     * @return
     * The errormessage
     */
    public String getErrormessage() {
        return errormessage;
    }

    /**
     *
     * @param errormessage
     * The errormessage
     */
    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    /**
     *
     * @return
     * The sections
     */
    public List<Section> getSections() {
        return sections;
    }

    /**
     *
     * @param sections
     * The sections
     */
    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

}
