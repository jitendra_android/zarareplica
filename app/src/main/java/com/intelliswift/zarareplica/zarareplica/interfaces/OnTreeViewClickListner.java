package com.intelliswift.zarareplica.zarareplica.interfaces;

/**
 * Created by Swapnil on 1/22/2015.
 */
public interface OnTreeViewClickListner {

    public void onTreeViewItemClick(int levelId, int itemId);
}
