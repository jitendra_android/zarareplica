package com.intelliswift.zarareplica.zarareplica.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.interfaces.OnTreeViewClickListner;
import com.intelliswift.zarareplica.zarareplica.ui.fragment.ProductSectionFragment;
import com.intelliswift.zarareplica.zarareplica.utility.treeview.AbstractTreeViewAdapter;
import com.intelliswift.zarareplica.zarareplica.utility.treeview.TreeNodeInfo;
import com.intelliswift.zarareplica.zarareplica.utility.treeview.TreeStateManager;
import com.squareup.picasso.Picasso;

public class ThreeLevelAdapter extends AbstractTreeViewAdapter<Long> {

    private OnTreeViewClickListner mTreeViewClickListner;
    private static final String TAG = "ThreeLevelAdapter";

    private ImageView toggleBtn;

    private TextView txtGroupTitle, txtChildTitle;
    private ProductSectionFragment fragment;

    public ThreeLevelAdapter(ProductSectionFragment productSectionFragment,
                             final TreeStateManager<Long> treeStateManager,
                             final int numberOfLevels) {
        super(productSectionFragment.getActivity(), treeStateManager, numberOfLevels);
        this.fragment = productSectionFragment;

        this.mTreeViewClickListner = (OnTreeViewClickListner)productSectionFragment;

    }

    @Override
    public View getNewChildView(final TreeNodeInfo<Long> treeNodeInfo) {
        int level = treeNodeInfo.getLevel();
        LinearLayout viewLayout;
        if (level == 0) {
            viewLayout = (LinearLayout) getActivity().getLayoutInflater()
                    .inflate(R.layout.header, null);
            return updateView(viewLayout, treeNodeInfo);
        } else if (level == 1) {
            viewLayout = (LinearLayout) getActivity().getLayoutInflater()
                    .inflate(R.layout.groups, null);
            return updateView(viewLayout, treeNodeInfo);
        } else if (level == 2) {
            viewLayout = (LinearLayout) getActivity().getLayoutInflater()
                    .inflate(R.layout.childs, null);
            return updateView(viewLayout, treeNodeInfo);
        } else {
            viewLayout = (LinearLayout) getActivity().getLayoutInflater()
                    .inflate(R.layout.childs, null);
            return updateView(viewLayout, treeNodeInfo);
        }
    }

    @Override
    public LinearLayout updateView(final View view,
                                   final TreeNodeInfo<Long> treeNodeInfo) {

        int level = treeNodeInfo.getLevel();
        final LinearLayout viewLayout = (LinearLayout) view;
        if (level == 0) {
            //header
            long id = treeNodeInfo.getId();
            final TextView sectionTitle = (TextView) viewLayout.findViewById(R.id.headerTitle);
            sectionTitle.setText(ProductSectionFragment.header.get((int) id));
            final ImageView headerImage = (ImageView) viewLayout.findViewById(R.id.headerImage);
            String url[] = ProductSectionFragment.headerUrl.toArray(new String[ProductSectionFragment.headerUrl.size()]);
            Picasso.with(getActivity()).load(url[(int) id]).placeholder(R.drawable.placeholder).into(headerImage);

        } else if (level == 1) {
            // Group
            txtGroupTitle = (TextView) viewLayout.findViewById(R.id.groupTitle);
            final long id = treeNodeInfo.getId();
            txtGroupTitle.setText(treeNodeInfo.getDisplayName());
            txtGroupTitle.requestFocus();
            toggleBtn = (ImageView) view.findViewById(R.id.iv_toggleBtn);


            toggleBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getManager().getNodeInfo(id).isExpanded()) {
                        Log.e(TAG, "Expanded" + v);
                        ((ImageView) v).setImageResource(android.R.drawable.btn_plus);
                    } else {
                        Log.e(TAG, "Collapsed");
                        ((ImageView) v).setImageResource(android.R.drawable.btn_minus);
                    }
                    expandCollapse(id);

                }
            });


        } else {
            // Child
            txtChildTitle = (TextView) viewLayout.findViewById(R.id.childTitle);
            long id = treeNodeInfo.getId();
            txtChildTitle.setText(treeNodeInfo.getDisplayName());
            txtChildTitle.requestFocus();
        }
        return viewLayout;
    }

    @Override
    public void handleItemClick(final View view, final Object id) {
        final Long longId = (Long) id;
        Log.d(TAG, "ClickedId: " + getClickedId(longId));
        Log.d(TAG, "Level : " + getManager().getNodeInfo(longId).getLevel());
        mTreeViewClickListner.onTreeViewItemClick(getManager().getNodeInfo(longId).getLevel(), getClickedId(longId));
        if (getManager().getNodeInfo(longId).isExpanded()) {
            if (getManager().getNodeInfo(longId).getLevel() == 0) {

                if (toggleBtn != null) {
                    toggleBtn.setImageResource(android.R.drawable.btn_plus);
                }
                collapseAll();


            }
        } else {
            if (getManager().getNodeInfo(longId).getLevel() != 1) {

                if (toggleBtn != null) {
                    toggleBtn.setImageResource(android.R.drawable.btn_plus);
                }

                collapseAll();


            }

            if (getManager().getNodeInfo(longId).getLevel() == 0 || getManager().getNodeInfo(longId).getLevel() == 2) {

                expandCollapse(longId);


            }
            if (getManager().getNodeInfo(longId).getLevel() == 2) {
                fragment.loadGridFragment();
            }

        }


    }

    @Override
    public long getItemId(final int position) {
        return getTreeId(position);
    }

    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

}