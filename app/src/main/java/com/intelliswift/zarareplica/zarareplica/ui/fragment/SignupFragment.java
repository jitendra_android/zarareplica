package com.intelliswift.zarareplica.zarareplica.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.ui.activity.BaseActivity;
import com.intelliswift.zarareplicacore.core.IOCContainer;
import com.intelliswift.zarareplicacore.core.IResponseSubscribe;
import com.intelliswift.zarareplicacore.core.model.user.ProfileInfoResponse;
import com.intelliswift.zarareplicacore.core.user.UserFacade;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class SignupFragment extends Fragment implements OnClickListener, IResponseSubscribe {

    BaseActivity app;

    @InjectView(R.id.signup_btn_cancel)
    Button mSignupBtnCancel;
    @InjectView(R.id.et_email)
    EditText mEtEmail;
    @InjectView(R.id.et_password)
    EditText mEtPassword;
    @InjectView(R.id.et_password_verify)
    EditText mEtPasswordVerify;
    @InjectView(R.id.et_fname)
    EditText mEtfName;
    @InjectView(R.id.et_postcode)
    EditText mEtPostcode;
    @InjectView(R.id.ck_eula)
    CheckBox mCkEula;
    @InjectView(R.id.ck_pt)
    CheckBox mCkPt;
    @InjectView(R.id.login_btn)
    Button mLoginBtn;
    @InjectView(R.id.et_lname)
    EditText mEtLname;
    @InjectView(R.id.et_address)
    EditText mEtAddress;
    @InjectView(R.id.et_country)
    EditText mEtCountry;
    @InjectView(R.id.et_state)
    EditText mEtState;
    @InjectView(R.id.et_city)
    EditText mEtCity;

    private UserFacade mUserFacade;

    boolean flag_isInfo = false;
    private String TAG = "UserInfoFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_signup, null);
        ButterKnife.inject(this, root);
        app = (BaseActivity) getActivity();


//        Button createAccountBtn = (Button) root.findViewById(R.id.create_account_btn);
//        createAccountBtn.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                app.switchFragment(new ForgetPasswordFragment(), true, R.id.container, false);
//            }
//        });

        flag_isInfo = getArguments().getBoolean("flag");
        if (flag_isInfo) {
            mUserFacade = (UserFacade) IOCContainer.getInstance(getActivity()).getObject(IOCContainer.ObjectName.USER_SERVICE, TAG);
            mUserFacade.getUserProfileInfo("2");
        }
        mSignupBtnCancel.setOnClickListener(this);

        return root;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signup_btn_cancel:
                getActivity().getFragmentManager().popBackStack();
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IOCContainer.getInstance(getActivity()).publisher.registerResponseSubscribe(this);
    }

    @Override
    public void onResponse(Object response, String tag) {
        if (response instanceof ProfileInfoResponse) {
            ProfileInfoResponse profileResponse = (ProfileInfoResponse) response;
            if (profileResponse.getErrorcode().equals("0")) {
                Toast.makeText(getActivity(), profileResponse.getPersonalDetails().toString(), Toast.LENGTH_SHORT).show();
                setDataToFields(profileResponse);
            }
        }
    }

    private void setDataToFields(ProfileInfoResponse profileResponse) {
        mEtEmail.setText(profileResponse.getPersonalDetails().get(0).getEmail());
        mEtfName.setText(profileResponse.getPersonalDetails().get(0).getFirstname() + " " + profileResponse.getPersonalDetails().get(0).getLastname());
        mEtPostcode.setText(profileResponse.getPersonalDetails().get(0).getPostcode());
    }

    @Override
    public void onErrorResponse(Exception error) {

    }
}
