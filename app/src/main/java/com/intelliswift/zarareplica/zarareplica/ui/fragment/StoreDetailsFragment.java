package com.intelliswift.zarareplica.zarareplica.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplicacore.core.model.stores.Store;
import com.kedzie.drawer.DragLayout;
import com.kedzie.drawer.DraggedDrawer;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Fragment displays details of the selected store
 */
public class StoreDetailsFragment extends Fragment implements View.OnClickListener {


    @InjectView(R.id.store_details_tv_city)
    TextView mTvCity;
    @InjectView(R.id.store_details_tv_name)
    TextView mTvName;
    @InjectView(R.id.store_details_tv_desc)
    TextView mTvDesc;
    @InjectView(R.id.store_details_tv_time)
    TextView mTvTime;
    @InjectView(R.id.store_details_tv_seven_day_timing)
    TextView mTvSevenDayTiming;
    @InjectView(R.id.store_details_tv_contact_no)
    TextView mTvContactNo;
    @InjectView(R.id.activity_maps)
    DragLayout mActivityMaps;
    @InjectView(R.id.store_details_get_direction)
    Button mStoreDetailsGetDirection;
    @InjectView(R.id.top_content)
    LinearLayout mTopContent;
    @InjectView(R.id.top_handle)
    ImageView mTopHandle;
    @InjectView(R.id.top)
    DraggedDrawer mTop;
    private DragLayout mLayout;
    private DraggedDrawer mTopDrawer;
    private ImageView mDrawerHandle;
    private String TAG = "Store Details Fragment";
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    private Store store;
//    private OnFragmentChangeListner mListener;

    private double mStoreLatititue, mStoreLongitude;

    public StoreDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_store_details, container, false);
        // Initializing butterknife
        ButterKnife.inject(this, rootView);

        store = (Store) getArguments().get("store_detail");

        mLayout = (DragLayout) rootView.findViewById(R.id.activity_maps);
        mTvSevenDayTiming.setOnClickListener(this);
        mTvContactNo.setOnClickListener(this);

        setDataToComponenets();
        setUpMapIfNeeded();

        // Drawer code
        mTopDrawer = (DraggedDrawer) rootView.findViewById(R.id.top);
        mDrawerHandle = (ImageView) rootView.findViewById(R.id.top_handle);
        mTopDrawer.setEdgeDraggable(true);
        mTopDrawer.setClickable(true);
        // Keep drawer by default open
        mLayout.openDrawer(mTopDrawer, true);
        mDrawerHandle.setOnClickListener(this);
        mStoreDetailsGetDirection.setOnClickListener(this);

        return rootView;
    }

    private void setDataToComponenets() {
        mTvCity.setText(store.getCity());
        mTvName.setText(store.getName());
        mTvDesc.setText(store.getDetails());
        mTvTime.setText(store.getTodaystiming());
        mTvContactNo.setText(store.getContactnumber().get(0));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentChangeListner) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnStoreLocationSelectedListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
                mStoreLatititue = Double.parseDouble(store.getLattitude());
                mStoreLongitude = Double.parseDouble(store.getLongitude());
                LatLng latLng = new LatLng(mStoreLatititue,mStoreLongitude);

                mMap.addMarker(new MarkerOptions()
                        .position(latLng));
                LatLng newLatLng = new LatLng(mStoreLatititue + 0.4, mStoreLongitude);
                CameraPosition cameraPosition = new CameraPosition.Builder().target(newLatLng).zoom(8f).build();
                CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                mMap.moveCamera(cameraUpdate);

            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.store_details_tv_seven_day_timing:
                // Displays 7 days timing of store
                //TODO Open new fragment and display 7 day timings
                break;

            case R.id.store_details_tv_contact_no:
                // Call to the contact no
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + mTvContactNo.getText().toString()));
                startActivity(callIntent);
                break;

            case R.id.top_handle:
                // Open and close drawer on clicking handle
                if (mLayout.isDrawerVisible(mTopDrawer)) {
                    mLayout.closeDrawer(mTopDrawer, true);
                } else {
                    mLayout.openDrawer(mTopDrawer, true);
                }
                break;

            case R.id.store_details_get_direction:
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?&daddr="+mStoreLatititue+","+mStoreLongitude+""));
                startActivity(intent);
                break;
        }
    }

}
