package com.intelliswift.zarareplica.zarareplica.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.interfaces.OnFragmentChangeListner;
import com.intelliswift.zarareplica.zarareplica.interfaces.OnTreeViewClickListner;
import com.intelliswift.zarareplica.zarareplica.ui.adapter.ThreeLevelAdapter;
import com.intelliswift.zarareplica.zarareplica.utility.treeview.InMemoryTreeStateManager;
import com.intelliswift.zarareplica.zarareplica.utility.treeview.TreeBuilder;
import com.intelliswift.zarareplica.zarareplica.utility.treeview.TreeStateManager;
import com.intelliswift.zarareplica.zarareplica.utility.treeview.TreeViewList;
import com.intelliswift.zarareplicacore.core.IOCContainer;
import com.intelliswift.zarareplicacore.core.IResponseSubscribe;
import com.intelliswift.zarareplicacore.core.model.section.Category;
import com.intelliswift.zarareplicacore.core.model.section.Section;
import com.intelliswift.zarareplicacore.core.model.section.SectionResponse;
import com.intelliswift.zarareplicacore.core.model.section.Subcategory;
import com.intelliswift.zarareplicacore.core.products.ProductFacade;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Fragment displays List of categories
 */
public class ProductSectionFragment extends Fragment implements IResponseSubscribe, TextView.OnEditorActionListener, OnTreeViewClickListner {
    public static List<String> header;

    public static List<String> headerUrl;

    private final Set<Long> selected = new HashSet<Long>();

    @InjectView(R.id.progressBar2)
    ProgressBar mProgressBar2;

    private TreeViewList treeView;

    private static final int LEVEL_NUMBER = 4;

    private TreeStateManager<Long> manager = null;

    private TreeBuilder<Long> treeBuilder = null;

    private ThreeLevelAdapter threeLevelAdapter;

    private String TAG = "ProductSectionFragment";

    private ProductFacade productFacade;

    private static SectionResponse sectionResponse;

    private OnFragmentChangeListner mFragmentChange;

    private EditText edtSearch;

    private OnTreeViewClickListner onTreeViewClickListner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IOCContainer.getInstance(getActivity()).publisher.registerResponseSubscribe(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_product_section, container, false);
        ButterKnife.inject(this, view);
        mProgressBar2.setVisibility(View.VISIBLE);
        productFacade = (ProductFacade) IOCContainer.getInstance(getActivity()).getObject(IOCContainer.ObjectName.PRODUCT_SERVICE, TAG);
        productFacade.getProductSectionList();


        manager = new InMemoryTreeStateManager<Long>();
        treeBuilder = new TreeBuilder<Long>(manager);
        treeView = (TreeViewList) view.findViewById(R.id.mainTreeView);

        edtSearch = (EditText) view.findViewById(R.id.sv_globalSearch);
        edtSearch.setOnEditorActionListener(this);

        threeLevelAdapter = new ThreeLevelAdapter(this, manager, LEVEL_NUMBER);
        treeView.setAdapter(threeLevelAdapter);

        mProgressBar2.setVisibility(View.VISIBLE);
//        treeView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                ProductGridFragment fragment = new ProductGridFragment();
////                Bundle bundle = new Bundle();
////                bundle.putString("stores_data",mResponse);
////                fragment.setArguments(bundle);
//                // Send the event to the host activity
//                mFragmentChange.onFragmentChange(fragment, true);
//            }
//        });

        return view;
    }

    public void loadGridFragment() {
        ProductGridFragment fragment = new ProductGridFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isSearch", false);
        fragment.setArguments(bundle);
        mFragmentChange.onFragmentChange(fragment, true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mFragmentChange = (OnFragmentChangeListner) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance(getActivity()).publisher.registerResponseSubscribe(this);

    }

    private void bindThreeLevelData(ArrayList<Section> sections) {
        int sectionIndex = 0;
        for (Section section : sections) {
            treeBuilder.sequentiallyAddNextNode((long) sectionIndex, 0, sectionIndex, section.getSectionName());

            List<Category> categories = new ArrayList<Category>(section.getCategories());
            int categoryIndex=0;
            for (Category category : categories) {
                treeBuilder.addRelation((long) sectionIndex, (long) Integer.parseInt(section.getSectionId() + category.getCategoryId()), Integer.parseInt(category.getCategoryId()), category.getCategoryName());
                List<Subcategory> subcategories = new ArrayList<Subcategory>(category.getSub_categories());
                for (Subcategory subcategory : subcategories) {
                    treeBuilder.addRelation((long) Integer.parseInt(section.getSectionId() + category.getCategoryId()), (long) Integer.parseInt(section.getSectionId() + category.getCategoryId() + subcategory.getSub_category_id()), Integer.parseInt(subcategory.getSub_category_id()), subcategory.getSub_category_name());
                }
            }
            manager.collapseChildren((long) sectionIndex);
            sectionIndex++;
        }
        threeLevelAdapter.notifyDataSetChanged();


    }

    @Override
    public void onResponse(Object response, String tag) {
//        try {
//            Log.d("Response String : ", new JSONObject(response).toString());
//        } catch (Exception e) {
//
//        }
//    }
        if (response instanceof SectionResponse) {
            sectionResponse = (SectionResponse) response;

            ArrayList<Section> sections = new ArrayList<Section>(sectionResponse.getSections());

            header = new ArrayList<String>();
            headerUrl = new ArrayList<String>();
            for (Section section : sections) {
                header.add(section.getSectionName());
                headerUrl.add(section.getImage());
            }

            bindThreeLevelData(sections);

        }
    }

//    @Override
//    public void onResponse(Object response, String tag) {
//        Log.d("Section Response : " , response.toString());
//        if (response instanceof SectionResponse) {
//            Toast.makeText(getActivity(), "onResponse called :"+ ((SectionResponse) response).getErrormessage(), Toast.LENGTH_LONG).show();
//        }
//
//    }

    @Override
    public void onErrorResponse(Exception error) {
        String e = error.toString();
        Toast.makeText(getActivity(),  e, Toast.LENGTH_SHORT).show();
    }

    // On search button clicked on the keyboard
    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//            performSearch();
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);

            ProductGridFragment fragment = new ProductGridFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isSearch", true);
            bundle.putString("search_String", edtSearch.getText().toString());
            fragment.setArguments(bundle);
            edtSearch.setText("");
            mFragmentChange.onFragmentChange(fragment, true);

            return true;
        }
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        treeBuilder.clear();
    }


    @Override
    public void onTreeViewItemClick(int levelId, int itemId) {
        Toast.makeText(getActivity(), "Level Id -" + levelId + ", Item id -" + itemId, Toast.LENGTH_SHORT).show();
//        ProductGridFragment fragment = new ProductGridFragment();
////                Bundle bundle = new Bundle();
////                bundle.putString("stores_data",mResponse);
////                fragment.setArguments(bundle);
//        // Send the event to the host activity
//        mFragmentChange.onFragmentChange(fragment, true);
    }
}