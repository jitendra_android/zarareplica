package com.intelliswift.zarareplica.zarareplica.ui.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.interfaces.OnFragmentChangeListner;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class UserAccountFragment extends Fragment implements View.OnClickListener{


    @InjectView(R.id.layout_notification)
    LinearLayout mLayoutNotification;
    @InjectView(R.id.layout_orders_placed)
    LinearLayout mLayoutOrdersPlaced;
    @InjectView(R.id.layout_shipping_addr)
    LinearLayout mLayoutShippingAddr;
    @InjectView(R.id.layout_payment_data)
    LinearLayout mLayoutPaymentData;
    @InjectView(R.id.layout_gift_card)
    LinearLayout mLayoutGiftCard;
    @InjectView(R.id.layout_personal_details)
    LinearLayout mLayoutPersonalDetails;
    @InjectView(R.id.layout_access_details)
    LinearLayout mLayoutAccessDetails;

    private OnFragmentChangeListner mFragmentChange;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mFragmentChange = (OnFragmentChangeListner) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_user_account, container, false);
        ButterKnife.inject(this, rootView);
        mLayoutNotification.setOnClickListener(this);
        mLayoutOrdersPlaced.setOnClickListener(this);
        mLayoutShippingAddr.setOnClickListener(this);
        mLayoutPaymentData.setOnClickListener(this);
        mLayoutGiftCard.setOnClickListener(this);
        mLayoutPersonalDetails.setOnClickListener(this);
        mLayoutAccessDetails.setOnClickListener(this);
        return rootView;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.layout_notification:
                break;

            case R.id.layout_orders_placed:
                break;

            case R.id.layout_access_details:
                break;

            case R.id.layout_gift_card:
                break;

            case R.id.layout_payment_data:
                break;

            case R.id.layout_personal_details:
                SignupFragment mSignUpFragment = new SignupFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("flag", true);
                mSignUpFragment.setArguments(bundle);
                mFragmentChange.onFragmentChange(mSignUpFragment,false);
                break;

            case R.id.layout_shipping_addr:
                break;
        }
    }
}
