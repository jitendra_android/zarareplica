package com.intelliswift.zarareplica.zarareplica.ui.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.ui.utils.ConfirmProdAdapter;
import com.intelliswift.zarareplica.zarareplica.ui.utils.ConfirmProdInterface;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Neelesh on 11/17/2014.
 */
public class ConfirmProdFragment extends Fragment {
    ListView confirmProdFragList;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_orderconfirm, container, false);
        ImageView imgBtnback = (ImageView) v.findViewById(R.id.masterpay_back);
        confirmProdFragList = (ListView) v.findViewById(R.id.confirmprod_fraglist);

        imgBtnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        List<ConfirmProdInterface> items = new ArrayList<ConfirmProdInterface>();

        items.add(new Items("Standard"));
        items.add(new Separator("Payment"));
        items.add(new Items("EDIT PAYMENT METHOD"));
        items.add(new Separator("Products"));
        items.add(new ExpandItems("0"));
        items.add(new Separator("Taxes"));

        ConfirmProdAdapter confirmProdAdapter = new ConfirmProdAdapter(getActivity(), inflater, items);
        confirmProdFragList.setAdapter(confirmProdAdapter);

        //make the initial setup of list to show on load if any...

        //start the onItemClickListener
        confirmProdFragList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                switch (position) {
                    case 0:
                        //start fragment for address
                        break;
                    case 2:
                        //start fragment for payment method
                        break;
                    case 4:
                        //expand and collapse this item to show the purchased products
                        break;
                }
            }
        });
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    //To show separators in listview...
    public class Separator implements ConfirmProdInterface {

        private final String name;

        public Separator(String name) {
            this.name = name;
        }

        @Override
        public int getViewType() {
            return ConfirmProdAdapter.RowType.SEPARATOR.ordinal();
        }

        @Override
        public View getView(LayoutInflater inflater, View convertView) {
            if(convertView == null) {
                convertView = inflater.inflate(R.layout.confirmprod_listview_separator, null);
            }

            TextView separatorText = (TextView) convertView.findViewById(R.id.tv_separator);
            separatorText.setText(name);
            return convertView;
        }
    }

    //To show clickable items
    public class Items implements ConfirmProdInterface {

        private  final String name;

        public Items(String text1) {
            this.name = text1;

        }

        @Override
        public int getViewType() {
            return ConfirmProdAdapter.RowType.ITEM.ordinal();        }

        @Override
        public View getView(LayoutInflater inflater, View convertView) {

            if (convertView == null) {
                convertView = inflater.inflate(
                        android.R.layout.simple_list_item_1, null);
            }
            TextView text1 = (TextView) convertView
                    .findViewById(android.R.id.text1);

            text1.setText(name);
            return convertView;
        }
    }

    public class ExpandItems implements ConfirmProdInterface {

        String pruductCount;

        public ExpandItems(String productCount) {
            this.pruductCount = productCount;
        }

        @Override
        public int getViewType() {
            return ConfirmProdAdapter.RowType.ITEM.ordinal();
        }

        @Override
        public View getView(LayoutInflater inflater, View convertView) {
            if(convertView == null) {
                convertView = inflater.inflate(R.layout.confirmprod_itemexpand, null);
            }
            TextView prodCount = (TextView) convertView.findViewById(R.id.prod_count);

            return convertView;
        }
    }
}
