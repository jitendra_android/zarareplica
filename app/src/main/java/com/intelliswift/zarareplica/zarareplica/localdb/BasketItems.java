package com.intelliswift.zarareplica.zarareplica.localdb;


import android.content.Context;
import android.graphics.Bitmap;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.orm.SugarRecord;
import com.intelliswift.zarareplica.zarareplica.R;

/**
 * Created by Neelesh on 11/13/2014.
 */
public class BasketItems extends SugarRecord<BasketItems> {

    String prodTitle, refNum, size, imgUrl, itmQty, price, totalPrice;

    public BasketItems() {
        //do nothing
    }

    public BasketItems(String prodTitle, String refNum, String size, String imgUrl, String itmQty, String price, String totalPrice) {
            this.imgUrl = imgUrl;
        this.itmQty = itmQty;
        this.price = price;
        this.prodTitle = prodTitle;
        this.refNum = refNum;
        this.size = size;
        this.totalPrice = totalPrice;
    }



}
