package com.intelliswift.zarareplica.zarareplica.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplicacore.core.model.stores.Store;

import java.util.ArrayList;

/**
 * Created by Swapnil on 11/6/2014.
 */
public class StoresListAdapter extends BaseAdapter{

    private Context context;
    private ArrayList<Store> storeList;
    LayoutInflater mInflater;
    ViewHolderItem viewHolder;

    public StoresListAdapter(Context context, ArrayList<Store> storeListItems) {
        this.context = context;
        this.storeList = storeListItems;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return storeList.size();
    }

    @Override
    public Object getItem(int position) {
        return storeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_stores, null);
            // well set up the ViewHolder
            viewHolder = new ViewHolderItem();
            viewHolder.tvCity = (TextView) convertView
                    .findViewById(R.id.row_stores_city);
            viewHolder.tvName = (TextView) convertView
                    .findViewById(R.id.row_stores_location);
            viewHolder.tvDetails = (TextView) convertView
                    .findViewById(R.id.row_stores_categories);
            viewHolder.tvDistance = (TextView) convertView
                    .findViewById(R.id.row_stores_distance);

            // store the holder with the view.
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }

        viewHolder.tvCity.setText(storeList.get(position).getCity());
        viewHolder.tvName.setText(storeList.get(position).getName());
        viewHolder.tvDetails.setText(storeList.get(position).getDetails());
        viewHolder.tvDistance.setText("20.00");// calculate distance and set it here


        return convertView;
    }

    // our ViewHolder.
    // caches our TextView
    static class ViewHolderItem {
        TextView tvCity;
        TextView tvName;
        TextView tvDetails;
        TextView tvDistance;
    }
}
