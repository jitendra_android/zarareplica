package com.intelliswift.zarareplica.zarareplica.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplicacore.core.model.productsearch.SearchItem;

import java.util.ArrayList;

/**
 * Created by Swapnil on 12/12/2014.
 */
public class ProductSearchResultListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<SearchItem> mList;
    LayoutInflater mInflater;
    ViewHolderItem viewHolder;

    public ProductSearchResultListAdapter(Context context, ArrayList<SearchItem> products) {
        this.mContext = context;
        this.mList = products;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_product_search_category, null);
            // well set up the ViewHolder
            viewHolder = new ViewHolderItem();
            viewHolder.tvCategory = (TextView) convertView
                    .findViewById(R.id.row_psr_tv_category);
            viewHolder.tvCount = (TextView) convertView
                    .findViewById(R.id.row_psr_tv_count);
            // store the holder with the view.
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }
        viewHolder.tvCategory.setText(mList.get(position).getSectionName());
        viewHolder.tvCount.setText(mList.get(position).getTotalItems()+"");
        return convertView;
    }

    // our ViewHolder.
    // caches our TextView
    static class ViewHolderItem {
        TextView tvCategory;
        TextView tvCount;
    }


}
