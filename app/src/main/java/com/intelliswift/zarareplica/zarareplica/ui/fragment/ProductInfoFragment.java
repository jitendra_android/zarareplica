package com.intelliswift.zarareplica.zarareplica.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.ui.adapter.ProductDetailPagerAdapter;


public class ProductInfoFragment extends Fragment {

    public static final int NUM_ITEMS = 10;
    ProductDetailPagerAdapter mAdapter;
    ViewPager mPager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View layout  = inflater.inflate(R.layout.fragment_product, container, false);

      //  setContentView(R.layout.fragment_pager);

        mAdapter = new ProductDetailPagerAdapter(getActivity().getSupportFragmentManager());
        mPager = (ViewPager)layout.findViewById(R.id.pager);
        mPager.setOffscreenPageLimit(2);
        mPager.setAdapter(mAdapter);

        return layout;

    }

}
