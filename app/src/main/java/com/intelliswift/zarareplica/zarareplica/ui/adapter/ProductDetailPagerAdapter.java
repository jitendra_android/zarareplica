package com.intelliswift.zarareplica.zarareplica.ui.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.intelliswift.zarareplica.zarareplica.ui.fragment.ProductDetailsFragment;
import com.intelliswift.zarareplica.zarareplica.ui.fragment.ProductInfoFragment;

public class ProductDetailPagerAdapter extends FragmentStatePagerAdapter {
    public ProductDetailPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return ProductInfoFragment.NUM_ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        return ProductDetailsFragment.newInstance(position);
    }
}
