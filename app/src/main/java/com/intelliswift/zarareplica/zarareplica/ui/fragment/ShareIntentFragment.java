package com.intelliswift.zarareplica.zarareplica.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intelliswift.zarareplica.zarareplica.R;


public class ShareIntentFragment extends Fragment {

    public static Fragment newInstance(int num) {
    	ShareIntentFragment f = new ShareIntentFragment();
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);

        return f;
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {

    	View view = inflater.inflate(R.layout.fragment_share_intent, null);

        return view;
    }
}
