package com.intelliswift.zarareplica.zarareplica.ui.utils;

import android.content.Context;
import android.view.View;
import android.widget.TabHost.TabContentFactory;

public class TabContents implements TabContentFactory {

	private Context context;

	public TabContents(Context context) {
		this.context = context;
	}
	
	@Override
	public View createTabContent(String tag) {
		View v = new View(context);
		return v;
	}

}
