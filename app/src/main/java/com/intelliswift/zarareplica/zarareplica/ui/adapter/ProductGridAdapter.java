package com.intelliswift.zarareplica.zarareplica.ui.adapter;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.constants.Constants;
import com.intelliswift.zarareplicacore.core.model.productlist.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Swapnil on 12/1/2014.
 */
public class ProductGridAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Product> mList;
    LayoutInflater mInflater;
    ViewHolderItem viewHolder;

    public ProductGridAdapter(Context context, ArrayList<Product> products) {
//        super(context,0,products);
        this.mContext = context;
        this.mList = products;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_product_grid, null);
            // well set up the ViewHolder
            viewHolder = new ViewHolderItem();
            viewHolder.ivProductImage = (ImageView) convertView
                    .findViewById(R.id.product_grid_iv_image);
            viewHolder.tvProductName = (TextView) convertView
                    .findViewById(R.id.product_grid_tv_name);
            viewHolder.tvProductPrice = (TextView) convertView
                    .findViewById(R.id.product_grid_tv_price);
            // store the holder with the view.
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }
        // Changing the size of image in Imageview
        DisplayMetrics metrics = new DisplayMetrics();

        float logicalDensity = mContext.getResources().getDisplayMetrics().density;
        int px = 0;
        if (Constants.productGridSize == 2) {
            viewHolder.tvProductName.setVisibility(View.VISIBLE);
            viewHolder.tvProductPrice.setVisibility(View.VISIBLE);
            viewHolder.tvProductName.setText(mList.get(position).getProductName());
            viewHolder.tvProductPrice.setText(mList.get(position).getProductPrize());
            px = (int) Math.ceil(200 * logicalDensity);
            Picasso.with(mContext).load(mList.get(position).getProductUrl()).placeholder(R.drawable.placeholder).resize(px, px).into(viewHolder.ivProductImage);
        } else {
            viewHolder.tvProductName.setVisibility(View.GONE);
            viewHolder.tvProductPrice.setVisibility(View.GONE);
            px = (int) Math.ceil(100 * logicalDensity);
            Picasso.with(mContext).load(mList.get(position).getProductUrl()).placeholder(R.drawable.placeholder).resize(px, px).into(viewHolder.ivProductImage);


        }


        return convertView;
    }

    // our ViewHolder.
    // caches our TextView
    static class ViewHolderItem {
        TextView tvProductName;
        TextView tvProductPrice;
        ImageView ivProductImage;
    }

}
