package com.intelliswift.zarareplica.zarareplica.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.gson.Gson;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.interfaces.OnFragmentChangeListner;
import com.intelliswift.zarareplica.zarareplica.ui.model.NearbyStoreLocation;
import com.intelliswift.zarareplicacore.core.model.stores.Store;
import com.intelliswift.zarareplicacore.core.model.stores.StoreResponse;

/**
 * Displays Nearby locations on map
 */

public class StoresNearbyFragment extends Fragment implements ClusterManager.OnClusterClickListener<NearbyStoreLocation>, ClusterManager.OnClusterInfoWindowClickListener<NearbyStoreLocation>, ClusterManager.OnClusterItemClickListener<NearbyStoreLocation>, ClusterManager.OnClusterItemInfoWindowClickListener<NearbyStoreLocation> {


    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    // Cluster manager is used to hold markers
    private ClusterManager<NearbyStoreLocation> mClusterManager;
    private StoreResponse storeResponse;

    private OnFragmentChangeListner mFragmentChange;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_stores_nearby, container, false);
        // Set up maps for cluster
        setUpMapIfNeeded();

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mFragmentChange = (OnFragmentChangeListner) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_nearby_stores))
                    .getMap();
            mMap.setMyLocationEnabled(true);
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpClusterer();
                //setUpMap();
            }
        }
    }

    private void setUpClusterer() {


        mClusterManager = new ClusterManager<NearbyStoreLocation>(getActivity(), mMap);
        mClusterManager.setRenderer(new CustomRenderer());
        mMap.setOnCameraChangeListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);
        addItems();

        mClusterManager.cluster();
    }

    private void addItems() {
        StoreResponse storeResponse = getArguments().getParcelable("stores_data");

        if (storeResponse.getErrorcode().equals("0")) {
            Store mStore;
            int size = storeResponse.getStore().size();
            NearbyStoreLocation location = null;
            for (int index = 0; index < size; index++) {
                double lat = Double.parseDouble(storeResponse.getStore().get(index).getLattitude());
                double lng = Double.parseDouble(storeResponse.getStore().get(index).getLongitude());
                location = new NearbyStoreLocation(lat, lng);
                mClusterManager.addItem(location);
                mStore = storeResponse.getStore().get(index);
                Log.d("Response", mStore.toString());

            }

        } else {
            // TODO : Define alertbao or show error for data response
        }


        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);

    }

    @Override
    public boolean onClusterClick(Cluster<NearbyStoreLocation> cluster) {
        Toast.makeText(getActivity(), "cluster cliked", Toast.LENGTH_LONG).show();
        return true;
    }

    @Override
    public boolean onClusterItemClick(NearbyStoreLocation item) {
        // Toast.makeText(getActivity(), "Item cliked", Toast.LENGTH_LONG).show();
        int size = storeResponse.getStore().size();


        NearbyStoreLocation location = null;
        for (int index = 0; index < size; index++) {
            double lat = Double.parseDouble(storeResponse.getStore().get(index).getLattitude());
            double lng = Double.parseDouble(storeResponse.getStore().get(index).getLongitude());
            location = new NearbyStoreLocation(lat, lng);
             if (location.equals(item)){
                Fragment fragment = new StoreDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable("store_detail", storeResponse.getStore().get(index));
                fragment.setArguments(bundle);

                // Send the event to the host activity
                mFragmentChange.onFragmentChange(fragment,false);
            }

        }


        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<NearbyStoreLocation> cluster) {
        Toast.makeText(getActivity(), "Item cliked", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClusterItemInfoWindowClick(NearbyStoreLocation item) {
        Toast.makeText(getActivity(), "Item cliked", Toast.LENGTH_LONG).show();
    }

    class CustomRenderer extends DefaultClusterRenderer<NearbyStoreLocation> {
        public CustomRenderer() {
            super(getActivity(), mMap, mClusterManager);
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster<NearbyStoreLocation> cluster) {
            //start clustering if at least 2 items overlap
            return cluster.getSize() > 1;
        }


    }


}
