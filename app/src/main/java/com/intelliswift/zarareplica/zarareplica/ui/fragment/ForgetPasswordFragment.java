package com.intelliswift.zarareplica.zarareplica.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.ui.activity.BaseActivity;

public class ForgetPasswordFragment extends Fragment

{

	BaseActivity app ;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View root = inflater.inflate(R.layout.fragment_forget_password, null);
	
		app = (BaseActivity) getActivity();
//		Button createAccountBtn = (Button)root.findViewById(R.id.create_account_btn);
//		createAccountBtn.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				app.switchFragment(new ForgetPasswordFragment(), false, R.id.content_frame, false);
//			}
//		});
		
		return root;

	}
}
