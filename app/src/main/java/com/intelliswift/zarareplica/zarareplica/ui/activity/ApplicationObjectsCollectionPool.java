package com.intelliswift.zarareplica.zarareplica.ui.activity;

import java.util.HashMap;

public class ApplicationObjectsCollectionPool {

	private static ApplicationObjectsCollectionPool instance;
	private HashMap<String, Object> pool;

	private ApplicationObjectsCollectionPool() {
		pool = new HashMap<String, Object>();
	}

	public static ApplicationObjectsCollectionPool getInstance() {

		if (instance == null) {
			instance = new ApplicationObjectsCollectionPool();

		}

		return instance;
	}

	public void clearCollectionPool() {
		pool.clear();
	}

	public void put(String key, Object value) {
		pool.put(key, value);
	}

	public Object get(String key) {
		return pool.get(key);
	}

	public void removeObject(String key) {

		if ((pool.get(key)) != null)
			pool.remove(key);

	}
}
