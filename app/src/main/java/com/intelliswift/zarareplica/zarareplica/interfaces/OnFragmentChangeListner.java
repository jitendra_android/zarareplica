package com.intelliswift.zarareplica.zarareplica.interfaces;

import android.support.v4.app.Fragment;

/**
 *   Interface to listen for changing of fragments
 *
 */
public interface OnFragmentChangeListner {
        // TODO: Update argument type and name
        public void onFragmentChange(Fragment fragmentObj,boolean addToBack);
    }