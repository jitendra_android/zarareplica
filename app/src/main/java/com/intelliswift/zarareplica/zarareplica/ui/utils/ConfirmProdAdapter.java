package com.intelliswift.zarareplica.zarareplica.ui.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created by Neelesh on 11/20/2014.
 */
public class ConfirmProdAdapter extends ArrayAdapter<ConfirmProdInterface> {

    private List<ConfirmProdInterface> items;
    private LayoutInflater inflater;

    public enum RowType {
        // Types of items to be present in list
        ITEM, SEPARATOR, EXPAND_ITEM
    }

    public ConfirmProdAdapter(Context context, LayoutInflater inflater,
                        List<ConfirmProdInterface> items) {
        super(context, 0, items);
        this.items = items;
        this.inflater = inflater;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return items.get(position).getView(inflater, convertView);
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getViewType();
    }

    @Override
    public int getViewTypeCount() {
        return RowType.values().length;
    }

    @Override
    public boolean isEnabled(int position) {
        if(position == 1 || position == 3 || position == 5) {
            return false;
        } else
        return super.isEnabled(position);
    }
}
