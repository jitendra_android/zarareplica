package com.intelliswift.zarareplica.zarareplica.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.FloatMath;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.constants.Constants;
import com.intelliswift.zarareplica.zarareplica.interfaces.OnFragmentChangeListner;
import com.intelliswift.zarareplica.zarareplica.ui.adapter.ProductGridAdapter;
import com.intelliswift.zarareplica.zarareplica.ui.adapter.ProductSearchResultListAdapter;
import com.intelliswift.zarareplicacore.core.IOCContainer;
import com.intelliswift.zarareplicacore.core.IResponseSubscribe;
import com.intelliswift.zarareplicacore.core.model.productlist.Product;
import com.intelliswift.zarareplicacore.core.model.productlist.ProductListResponse;
import com.intelliswift.zarareplicacore.core.model.productsearch.ProductSerachResponse;
import com.intelliswift.zarareplicacore.core.model.productsearch.SearchItem;
import com.intelliswift.zarareplicacore.core.products.ProductFacade;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;


/**
 * Fragment displays Grid of products
 */
public class ProductGridFragment extends Fragment implements View.OnTouchListener, IResponseSubscribe, View.OnClickListener, AdapterView.OnItemClickListener {

    @InjectView(R.id.gridView)
    GridView mGridView;
    @InjectView(R.id.progressBar)
    ProgressBar mProgressBar;
    @InjectView(R.id.sv_globalSearch)
    TextView mSvGlobalSearch;
    @InjectView(R.id.pg_btn_search_cancel)
    Button mPgBtnSearchCancel;
    @InjectView(R.id.product_grid_listview)
    ListView mProductGridListview;
    @InjectView(R.id.empty)
    Button mEmpty;
    @InjectView(R.id.product_grid_list_layout)
    LinearLayout mProductGridListLayout;
    @InjectView(R.id.product_grid_layout_search)
    LinearLayout mProductGridLayoutSearch;
    private OnFragmentChangeListner mListener;
    private ArrayList<Product> mProductList;
    private ArrayList<SearchItem> mSearchItems;
    private ProductGridAdapter mProductsGridAdapter;
    private ProductSearchResultListAdapter mProductSRLAdapter;


    private String mSearchString = null;

    private String TAG = "ProductListFragment";
    private ProductFacade productFacade;
    private ProductListResponse mProductListResponse;


    // These matrices will be used to move and zoom image
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();

    private Context mContext;

    // We can be in one of these 3 states
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    // Remember some things for zooming
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IOCContainer.getInstance(getActivity()).publisher.registerResponseSubscribe(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_product_grid, container, false);
        ButterKnife.inject(this, rootView);
        productFacade = (ProductFacade) IOCContainer.getInstance(getActivity()).getObject(IOCContainer.ObjectName.PRODUCT_SERVICE, TAG);
        // If page is not in search mode hide the layout
        mProductGridListLayout.setVisibility(View.GONE);
        // If page in search mode then visible the search layout and get products search results

        if (getArguments().getBoolean("isSearch")) {
            mSearchString = getArguments().getString("search_String");
            mSvGlobalSearch.setVisibility(View.VISIBLE);
            mPgBtnSearchCancel.setVisibility(View.VISIBLE);
            mProductGridLayoutSearch.setVisibility(View.VISIBLE);
            mSvGlobalSearch.setText(mSearchString);

            mSearchItems = new ArrayList<SearchItem>();
            productFacade.getSearchProductResult(mSearchString);
            mProductSRLAdapter = new ProductSearchResultListAdapter(mContext, mSearchItems);
            mProductGridListview.setAdapter(mProductSRLAdapter);
            mProductGridListview.setOnItemClickListener(this);
            //       mProductGridListview.setOnClickListener(this);
//            mProductGridListview.setVisibility(View.VISIBLE);
            mProductGridListLayout.setVisibility(View.VISIBLE);
            mEmpty.setOnClickListener(this);
        }else{
                  productFacade.getProductList();
        }

        mProgressBar.setVisibility(View.VISIBLE);
        mProductList = new ArrayList<Product>();
        mProductsGridAdapter = new ProductGridAdapter(mContext, mProductList);
        mGridView.setAdapter(mProductsGridAdapter);

        mSvGlobalSearch.setOnClickListener(this);
        // Get the date from Web Api


        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (mProductGridListLayout.getVisibility() == View.VISIBLE) {
                    mProductGridListLayout.setVisibility(View.GONE);
                } else {

                    ProductDetailsFragment fragment = new ProductDetailsFragment();
                    mListener.onFragmentChange(fragment, true);
                }
            }
        });


        mGridView.setOnTouchListener(this);

        mPgBtnSearchCancel.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance(getActivity()).publisher.registerResponseSubscribe(this);
  //      productFacade.getProductList();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;

        try {
            mListener = (OnFragmentChangeListner) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }


    @Override
    public boolean onTouch(View view, MotionEvent event) {
        // make the image scalable as a matrix
        float scale;

        // Handle touch events here...
        switch (event.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN: //first finger down only
                if (mProductGridListLayout.getVisibility() == View.VISIBLE) {
                    mProductGridListLayout.setVisibility(View.GONE);
                }
                savedMatrix.set(matrix);
                start.set(event.getX(), event.getY());
                mode = DRAG;
                break;
            case MotionEvent.ACTION_UP: //first finger lifted
            case MotionEvent.ACTION_POINTER_UP: //second finger lifted
                mode = NONE;
                break;
            case MotionEvent.ACTION_POINTER_DOWN: //second finger down
                oldDist = spacing(event); // calculates the distance between two points where user touched.
                // minimal distance between both the fingers
                if (oldDist > 5f) {
                    savedMatrix.set(matrix);
                    midPoint(mid, event); // sets the mid-point of the straight line between two points where user touched.
                    mode = ZOOM;
                }
                break;

            case MotionEvent.ACTION_MOVE:

                if (mode == DRAG) { //movement of first finger
                    matrix.set(savedMatrix);
                    if (view.getLeft() >= -392) {
                        matrix.postTranslate(event.getX() - start.x, event.getY() - start.y);
                    }
                } else if (mode == ZOOM) { //pinch zooming

                    float newDist = spacing(event);
                    if (newDist > oldDist) {
                        mode = DRAG;
                        mGridView.setNumColumns(2);
                        Constants.productGridSize = 2;
                        matrix.set(savedMatrix);
                        scale = newDist / oldDist; //thinking I need to play around with this value to limit it**
                        matrix.postScale(scale, scale, mid.x, mid.y);
                    } else if (oldDist > newDist) {
                        mode = DRAG;
                        mGridView.setNumColumns(4);
                        Constants.productGridSize = 4;
                        matrix.set(savedMatrix);
                        scale = newDist / oldDist; //thinking I need to play around with this value to limit it**
                        matrix.postScale(scale, scale, mid.x, mid.y);
                    }
                }
                break;
        }


        if (mode == ZOOM) {
            return true;
        }
        return false;
    }

    // Calculate the space between two touches
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return FloatMath.sqrt(x * x + y * y);
    }

    // Get the midpoint of touch
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }


    @Override
    public void onResponse(Object response, String tag) {

        if (response instanceof ProductListResponse) {
            mProductListResponse = (ProductListResponse) response;
            if (mProductListResponse.getErrorcode().equals("0")) {
                Product mProduct;
                mProductList.clear();
                int size = mProductListResponse.getProducts().size();
//                mProductList = new ArrayList<Product>();
                for (int index = 0; index < size; index++) {
                    mProduct = mProductListResponse.getProducts().get(index);
                    Log.d("Response", mProduct.toString());
                    mProductList.add(mProduct);
                }
            }
            mProductsGridAdapter.notifyDataSetChanged();
            if (mProgressBar != null)
                mProgressBar.setVisibility(View.INVISIBLE);
        } else if (response instanceof ProductSerachResponse) {
            ProductSerachResponse productSerachResponse = (ProductSerachResponse) response;
            int size = productSerachResponse.getSearchItem().size();
            SearchItem item;
            if(mSearchItems == null){
                mSearchItems = new ArrayList<SearchItem>();
            }
            mSearchItems.clear();
            for (int index = 0; index < size; index++) {
                item = new SearchItem();
                item.setSectionsId(productSerachResponse.getSearchItem().get(index).getSectionsId());
                item.setSectionName(productSerachResponse.getSearchItem().get(index).getSectionName());
                item.setTotalItems(productSerachResponse.getSearchItem().get(index).getTotalItems());
                mSearchItems.add(item);
            }
            mProductsGridAdapter.notifyDataSetChanged();

                Product mProduct;
                mProductList.clear();
                int prodSize = productSerachResponse.getProducts().size();
//                mProductList = new ArrayList<Product>();
                for (int index = 0; index < prodSize; index++) {

                    mProduct = productSerachResponse.getProducts().get(index);
                    Log.d("Response", mProduct.toString());
                    mProductList.add(mProduct);
                }

            mProductsGridAdapter.notifyDataSetChanged();

            if (mProgressBar != null)
                mProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onErrorResponse(Exception error) {
        // TODO : Define alertbao or show error for data response
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.pg_btn_search_cancel) {
            getActivity().onBackPressed();
        } else if (id == R.id.sv_globalSearch) {
            if (mProductGridListLayout.getVisibility() == View.GONE) {
                mProductGridListLayout.setVisibility(View.VISIBLE);
            }
        } else if (id == R.id.empty) {
//            Toast.makeText(mContext, "hello", Toast.LENGTH_SHORT).show();
            mProductGridListLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//        Toast.makeText(mContext, "" + position, Toast.LENGTH_SHORT).show();
        mProductGridListLayout.setVisibility(View.GONE);
        productFacade.getSearchProductResult(mSearchString);
    }
}
