package com.intelliswift.zarareplica.zarareplica.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.intelliswift.zarareplica.zarareplica.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase.*;


public class FullScreenImageAdapter extends PagerAdapter {
 
    private Activity _activity;
    private LayoutInflater inflater;
    List<String> urls;

    // constructor
    public FullScreenImageAdapter(Activity activity, List<String> urls) {
        this._activity = activity;
        this.urls = urls;
        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
 
    @Override
    public int getCount() {
        return this.urls.size();
    }
 
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }
     
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageViewTouch imgDisplay;
        Button btnClose;

        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
                false);

        imgDisplay = (ImageViewTouch) viewLayout.findViewById(R.id.imgDisplay);
        imgDisplay.setDisplayType(DisplayType.FIT_TO_SCREEN);
        imgDisplay.setScaleType(ImageView.ScaleType.MATRIX);

        ImageLoader.getInstance().displayImage(urls.get(position), imgDisplay);

        // close button click event
        ((ViewPager) container).addView(viewLayout);
  
        return viewLayout;
    }
     
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
  
    }
}
