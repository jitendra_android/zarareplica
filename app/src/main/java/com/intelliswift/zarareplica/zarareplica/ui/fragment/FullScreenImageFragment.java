package com.intelliswift.zarareplica.zarareplica.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.ui.activity.ApplicationObjectsCollectionPool;
import com.intelliswift.zarareplica.zarareplica.ui.activity.BaseActivity;
import com.intelliswift.zarareplica.zarareplica.ui.adapter.FullScreenImageAdapter;

import java.util.List;


public class FullScreenImageFragment extends Fragment {

	ApplicationObjectsCollectionPool pool;
	ViewPager viewPager;
	FullScreenImageAdapter adapter;
    BaseActivity app;

	@SuppressWarnings("unchecked")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View layout = inflater.inflate(R.layout.activity_fullscreen_view,
				container, false);
        app = (BaseActivity)getActivity();
		pool = ApplicationObjectsCollectionPool.getInstance();
		int position = Integer.parseInt(pool.get("position").toString());

        List<String> imageurls = (List)pool.get("list");

		viewPager = (ViewPager) layout.findViewById(R.id.pager);
		adapter = new FullScreenImageAdapter(getActivity(), imageurls);

		viewPager.setAdapter(adapter);

		// displaying selected image first
		viewPager.setCurrentItem(position);

		return layout;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		calledAfterViewInjection();
	}

	void calledAfterViewInjection() {}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

        //app.switchFragment(new ProductInfoFragment(), true, R.id.container, false);
	}

}
