package com.intelliswift.zarareplica.zarareplica.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.interfaces.OnFragmentChangeListner;
import com.intelliswift.zarareplica.zarareplica.ui.activity.BaseActivity;

public class LoginFragment extends Fragment {

	BaseActivity app ;
    private OnFragmentChangeListner mFragmentChange;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View root = inflater.inflate(R .layout.fragment_login, null);
	
		app = (BaseActivity) getActivity();
		
		
		
		Button loginBtn = (Button)root.findViewById(R.id.login_btn);
		loginBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
                UserAccountFragment fragment = new UserAccountFragment();
                mFragmentChange.onFragmentChange(fragment, true);
//				Intent in = new Intent(getActivity(), MainActivity.class);
//				getActivity().startActivity(in);
			}
		});


        TextView forgetPasswordTv = (TextView)root.findViewById(R.id.forgot_password);
        forgetPasswordTv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                app.switchFragment(new ForgetPasswordFragment(), true, R.id.container, false);
            }
        });


        Button createAccountBtn = (Button)root.findViewById(R.id.create_account_btn);
		createAccountBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				app.switchFragment(new SignupFragment(), true, R.id.container, false);
			}
		});
		
		

		return root;

	}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mFragmentChange = (OnFragmentChangeListner) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
}
