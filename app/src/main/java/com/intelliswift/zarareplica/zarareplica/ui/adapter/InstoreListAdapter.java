package com.intelliswift.zarareplica.zarareplica.ui.adapter;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.ui.utils.ViewHolder;
import com.intelliswift.zarareplicacore.core.model.productinfo.InstoreAvailability;

import java.util.List;

public class InstoreListAdapter extends BaseAdapter {
    List<InstoreAvailability> instoreList;
    Context ctx;
    LayoutInflater mInflater;

    public InstoreListAdapter(Context ctx, List<InstoreAvailability> instoreList){
        this.instoreList = instoreList;
        this.ctx = ctx;
        mInflater = LayoutInflater.from(ctx);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return instoreList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listitem_instore_list, null);
        }


        TextView title = ViewHolder.get(convertView, R.id.tv_item);
        title.setText(instoreList.get(position).getSize());
        TextView outOfStock = ViewHolder.get(convertView, R.id.tv_outofstock);

        if(Integer.parseInt(instoreList.get(position).getIsAvailable())== 1){
            convertView.setClickable(false);
            convertView.setBackgroundColor(ctx.getResources().getColor(R.color.white));
            outOfStock.setVisibility(View.INVISIBLE);

        } else {
            outOfStock.setVisibility(View.VISIBLE);
            convertView.setClickable(true);
            convertView.setBackgroundColor(ctx.getResources().getColor(R.color.grey));
        }

        return convertView;
    }


}
