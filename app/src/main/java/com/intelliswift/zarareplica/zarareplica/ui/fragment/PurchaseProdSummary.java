package com.intelliswift.zarareplica.zarareplica.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.intelliswift.zarareplica.zarareplica.R;

/**
 * Created by Neelesh on 11/19/2014.
 */
public class PurchaseProdSummary extends Fragment {

    public static final PurchaseProdSummary prodSummaryInstnace(int position) {
        PurchaseProdSummary prodSummary = new PurchaseProdSummary();
        Bundle bundle = new Bundle(1);
        bundle.putInt("position", position);
        prodSummary.setArguments(bundle);
        return prodSummary;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_purchaseprod_summary, container, false);
        RelativeLayout relativeLayout = (RelativeLayout) v.findViewById(R.id.prod_summary_hold);
        relativeLayout.setBackgroundResource(R.drawable.zara13);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
