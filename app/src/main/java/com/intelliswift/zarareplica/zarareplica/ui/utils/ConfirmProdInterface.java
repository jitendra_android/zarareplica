package com.intelliswift.zarareplica.zarareplica.ui.utils;

import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by Neelesh on 11/20/2014.
 */
public interface ConfirmProdInterface {

    public int getViewType();

    public View getView(LayoutInflater inflater, View convertView);
}
