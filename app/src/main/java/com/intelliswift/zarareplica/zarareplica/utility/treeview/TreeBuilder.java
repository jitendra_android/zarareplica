package com.intelliswift.zarareplica.zarareplica.utility.treeview;

public class TreeBuilder<T> {
    private final TreeStateManager<T> manager;

    private T lastAddedId = null;
    private int lastLevel = -1;

    public TreeBuilder(final TreeStateManager<T> manager) {
        this.manager = manager;
    }

    public void clear() {
        manager.clear();
        lastAddedId = null;
        lastLevel = -1;
    }

    public synchronized void addRelation(final T parent, final T child, int actualId, String displayName) {
        manager.addAfterChild(parent, child, null,actualId, displayName);
        lastAddedId = child;
        lastLevel = manager.getLevel(child);
    }

    public synchronized void sequentiallyAddNextNode(final T id, final int level, int actualId, String displayName) {
        if (lastAddedId == null) {
            addNodeToParentOneLevelDown(null, id, level,actualId, displayName);
        } else {
            if (level <= lastLevel) {
                final T parent = findParentAtLevel(lastAddedId, level - 1);
                addNodeToParentOneLevelDown(parent, id, level,actualId, displayName);
            } else {
                addNodeToParentOneLevelDown(lastAddedId, id, level,actualId, displayName);
            }
        }
    }

    private T findParentAtLevel(final T node, final int levelToFind) {
        T parent = manager.getParent(node);
        while (parent != null) {
            if (manager.getLevel(parent) == levelToFind) {
                break;
            }
            parent = manager.getParent(parent);
        }
        return parent;
    }

    private void addNodeToParentOneLevelDown(final T parent, final T id,
            final int level, int actualId, String displayName) {
        if (parent == null && level != 0) {
            throw new TreeConfigurationException("Trying to add new id " + id
                    + " to top level with level != 0 (" + level + ")");
        }
        if (parent != null && manager.getLevel(parent) != level - 1) {
            throw new TreeConfigurationException("Trying to add new id " + id
                    + " <" + level + "> to " + parent + " <"
                    + manager.getLevel(parent)
                    + ">. The difference in levels up is bigger than 1.");
        }
        manager.addAfterChild(parent, id, null,actualId,displayName);
        setLastAdded(id, level);
    }

    private void setLastAdded(final T id, final int level) {
        lastAddedId = id;
        lastLevel = level;
    }
}