package com.intelliswift.zarareplica.zarareplica.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.ui.activity.ApplicationObjectsCollectionPool;
import com.intelliswift.zarareplica.zarareplica.ui.activity.BaseActivity;
import com.intelliswift.zarareplica.zarareplica.ui.activity.MyActivity;
import com.intelliswift.zarareplicacore.core.IOCContainer;
import com.intelliswift.zarareplicacore.core.IResponseSubscribe;
import com.intelliswift.zarareplicacore.core.model.productinfo.ProductInfoResponse;
import com.intelliswift.zarareplicacore.core.products.ProductFacade;


public class ProductDetailsFragment extends Fragment implements IResponseSubscribe, BaseSliderView.OnSliderClickListener {

    private SliderLayout mDemoSlider;
    private ProductFacade productFacade;
    private String TAG = "ProductDetailsFragment";
    private ProductInfoResponse productInfoResponse;

    TextView tv_title;
    TextView tv_ref;
    TextView tv_price;
    TextView tv_des;
    MyActivity act;
    ApplicationObjectsCollectionPool pool;
    BaseActivity app;

    static Button notifCount;
    static int mNotifCount = 0;
    String productId = "135";

    public static Fragment newInstance(int num) {
        ProductDetailsFragment f = new ProductDetailsFragment();
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);

        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        act = (MyActivity)getActivity();
        act.hideTabHost(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Log.e("zararepilica ", "Product Details");
        //====================================
        productFacade = (ProductFacade) IOCContainer.getInstance(getActivity()).getObject(IOCContainer.ObjectName.PRODUCT_SERVICE, TAG);
        productFacade.getProductInfo(productId);
        //=====================================
        pool = ApplicationObjectsCollectionPool.getInstance();
        app = (BaseActivity)getActivity();
        View view = inflater.inflate(R.layout.fragment_product_details, null);
        mDemoSlider = (SliderLayout)view.findViewById(R.id.slider);

        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);

        tv_title = (TextView)view.findViewById(R.id.tv_title);
        tv_ref = (TextView)view.findViewById(R.id.tv_ref);

        tv_price = (TextView)view.findViewById(R.id.tv_price);
        tv_des = (TextView)view.findViewById(R.id.tv_des);

        Button add = (Button)view.findViewById(R.id.btn_add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openInstoreListDialoge();

            }
        });


        Button btn_share = (Button)view.findViewById(R.id.btn_share);
        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openShareIntentDialoge();
            }
        });

        return view;
    }

    public void openShareIntentDialoge(){

        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager()
                .beginTransaction().setCustomAnimations(R.anim.slide_up, R.anim.slide_down);

        fragmentTransaction.add(R.id.container, new ShareIntentFragment());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    public void openInstoreListDialoge(){
        FragmentManager manager = getFragmentManager();
        manager.beginTransaction().setCustomAnimations(R.anim.slide_up, R.anim.slide_down);
        InstoreListFragment instoreFragment = new InstoreListFragment();
        instoreFragment.setTargetFragment(this, 1);
        instoreFragment.show(manager, "InstoreFragment");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        // Stuff to do, dependent on requestCode and resultCode
        if(requestCode == 1)  // 1 is an arbitrary number, can be any int
        {
            // This is the return result of your DialogFragment
            if(resultCode == 1) // 1 is an arbitrary number, can be any int
            {
                Integer count = (Integer)pool.get("count");

                if(count != null)
                    setNotifCount(count);

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance(getActivity()).publisher.registerResponseSubscribe(this);

    }

    @Override
    public void onResponse(Object response, String tag) {

        if (response instanceof ProductInfoResponse) {

            productInfoResponse = (ProductInfoResponse) response;
            if(productInfoResponse.getErrorcode().equalsIgnoreCase("0")){

                pool.put("products_info", productInfoResponse.getProductsInfo());

                tv_title.setText(productInfoResponse.getProductsInfo().getInfo().getDetails().getTitle());
                tv_ref.setText(productInfoResponse.getProductsInfo().getInfo().getDetails().getReferNo());
                tv_price.setText(productInfoResponse.getProductsInfo().getProductPrize()+" $");
                tv_des.setText(productInfoResponse.getProductsInfo().getInfo().getDescription());

                int i = 0;
                for(String productImageUrl : productInfoResponse.getProductsInfo().getImageUrl())
                {
                    {
                        TextSliderView textSliderView = new TextSliderView(getActivity());
                        // initialize a SliderLayout
                        textSliderView
                                .description("")
                                .image(productImageUrl)
                                .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                                .setOnSliderClickListener(this);

                        //add your extra information
                        textSliderView.getBundle()
                                .putString("extra", ""+i);

                        mDemoSlider.addSlider(textSliderView);
                        i++;
                    }
                }
            }
        }
    }

    @Override
    public void onErrorResponse(Exception error) {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

        pool.put("position", slider.getBundle().get("extra"));
        pool.put("list",  productInfoResponse.getProductsInfo().getImageUrl());

        BaseActivity app = (BaseActivity)getActivity();
        app.switchFragment(new FullScreenImageFragment(), true, R.id.container, false);
//        Toast.makeText(getActivity(), slider.getBundle().get("extra") + "", Toast.LENGTH_SHORT).show();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.my, menu);
        View count = menu.findItem(R.id.badge).getActionView();
        notifCount = (Button) count.findViewById(R.id.notif_count);
        notifCount.setText(String.valueOf(mNotifCount));
        super.onCreateOptionsMenu(menu,inflater);
    }

    private void setNotifCount(int count){
        mNotifCount = count;
        getActivity().invalidateOptionsMenu();
    }
}
