package com.intelliswift.zarareplica.zarareplica.ui.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by Swapnil on 11/21/2014.
 */
public class NearbyStoreLocation implements ClusterItem {

    private final LatLng mPosition;

    public NearbyStoreLocation(double lat, double lng) {
        mPosition = new LatLng(lat, lng);
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof NearbyStoreLocation)) {
            return false;
        }
        NearbyStoreLocation storeLocation = (NearbyStoreLocation) o;
        if (storeLocation.getPosition().equals(mPosition)) {
            return true;
        }

        return false;
    }
}
