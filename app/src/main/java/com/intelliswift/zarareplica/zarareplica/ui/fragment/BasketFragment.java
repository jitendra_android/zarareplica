package com.intelliswift.zarareplica.zarareplica.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.Toast;

import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.localdb.BasketItems;
import com.intelliswift.zarareplica.zarareplica.ui.activity.MyActivity;
import com.intelliswift.zarareplica.zarareplica.ui.utils.ItemListAdapter;

import java.util.List;


public class BasketFragment extends android.support.v4.app.Fragment {
    Button btnClose, btnNext;
    public static SwipeListView itemList;
    ItemListAdapter itemListAdapter;
    List<BasketItems> purchaseList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        for(int i=0;i<5;i++) {
            BasketItems items = new BasketItems("Shirt", "1412", "M", "www.google.com", "1", "450", "450");
            items.save();
        }
        purchaseList = BasketItems.listAll(BasketItems.class);
        View v = inflater.inflate(R.layout.fragment_basket, container, false);
        itemListAdapter = new ItemListAdapter(getActivity());
        btnClose = (Button) v.findViewById(R.id.close_basket);
        btnNext = (Button) v.findViewById(R.id.btn_next);
        itemList = (SwipeListView) v.findViewById(R.id.swipe_view);
        itemList.setChoiceMode(SwipeListView.CHOICE_MODE_MULTIPLE_MODAL);

        itemList.setAdapter(itemListAdapter);
        itemList.setEmptyView(inflater.inflate(R.layout.empty_basket_layout, container, false));
        btnClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                MyActivity.mTabWidget.setVisibility(View.VISIBLE);
                MyActivity.mTabHost.setCurrentTab(MyActivity.currentTab);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConfirmProdFragment confirmProdFragment = new ConfirmProdFragment();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, confirmProdFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        itemList.setSwipeListViewListener(new BaseSwipeListViewListener() {
            @Override
            public void onOpened(int position, boolean toRight) {
                super.onOpened(position, toRight);
            }

            @Override
            public void onClosed(int position, boolean fromRight) {
                super.onClosed(position, fromRight);
            }

            @Override
            public void onListChanged() {
                super.onListChanged();
            }

            @Override
            public void onMove(int position, float x) {
                super.onMove(position, x);
            }

            @Override
            public void onStartOpen(int position, int action, boolean right) {
                Log.d("swipe", String.format("onStartOpen %d - action %d", position, action));
            }

            @Override
            public void onStartClose(int position, boolean right) {
                Log.d("swipe", String.format("onStartClose %d", position));
            }

            @Override
            public void onClickFrontView(int position) {

                PurchaseProdSummary prodSummary = PurchaseProdSummary.prodSummaryInstnace(position);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, prodSummary);
                transaction.addToBackStack(null);
                transaction.commit();

            }

            @Override
            public void onClickBackView(int position) {
                Log.d("swipe", String.format("onClickBackView %d", position));
            }

            @Override
            public void onDismiss(int[] reverseSortedPositions) {

                itemListAdapter.notifyDataSetChanged();
            }
        });

        return v;
    }

}
