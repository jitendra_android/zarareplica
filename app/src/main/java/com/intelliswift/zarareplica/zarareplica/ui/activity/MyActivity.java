package com.intelliswift.zarareplica.zarareplica.ui.activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TabWidget;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.interfaces.OnFragmentChangeListner;
import com.intelliswift.zarareplica.zarareplica.ui.fragment.BasketFragment;
import com.intelliswift.zarareplica.zarareplica.ui.fragment.LoginFragment;
import com.intelliswift.zarareplica.zarareplica.ui.fragment.ProductDetailsFragment;
import com.intelliswift.zarareplica.zarareplica.ui.fragment.ProductSectionFragment;
import com.intelliswift.zarareplica.zarareplica.ui.fragment.StoresFragment;


public class MyActivity extends BaseActivity implements OnFragmentChangeListner {
    public static int currentTab;
    public static FragmentTabHost mTabHost;
    public static TabWidget mTabWidget;
    public static FragmentManager fragmentManager;
    private FragmentTransaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        fragmentManager = getSupportFragmentManager();
        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabWidget = (TabWidget) findViewById(android.R.id.tabs);
        mTabHost.setup(this, fragmentManager, android.R.id.tabcontent);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_up);

        mTabHost.setAnimation(animation);

       /* TabHost.OnTabChangeListener tabChangeListener = new TabHost.OnTabChangeListener() {

            @Override
            public void onTabChanged(String tabId) {
                ProductFragment productFragment = (ProductFragment) getSupportFragmentManager().findFragmentByTag("producttab");
                BasketFragment basketFragment = (BasketFragment) getSupportFragmentManager().findFragmentByTag("baskettab");
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

               transaction.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);

                *//*if (productSectionFragment != null)
                    transaction.detach(productSectionFragment);
                if (basketFragment != null)
                    transaction.detach(basketFragment);
                if (storesFragment != null)
                    transaction.detach(storesFragment);


                if (tabId.equalsIgnoreCase("producttab")) {
                    if (productSectionFragment == null) {
                        transaction.add(R.id.container, new ProductSectionFragment(), "producttab");
                        currentTab = 0;
                    } else {
                        transaction.attach(productSectionFragment);
                        currentTab = 0;
                    }
                } else {
                    if (basketFragment == null) {
                        mTabWidget.setVisibility(View.GONE);
                        transaction.add(R.id.container, new BasketFragment(), "baskettab");
                    } else {
                        mTabWidget.setVisibility(View.GONE);
                        transaction.attach(basketFragment);
                    }
                }*//*
                transaction.commit();
            }
        };*/


        // mTabHost.setOnTabChangedListener(tabChangeListener);
         /* TabHost.TabSpec tabSpec1 = mTabHost.newTabSpec("producttab");
        TabHost.TabSpec tabSpec2 = mTabHost.newTabSpec("baskettab");
        tabSpec1.setContent(new TabContents(getBaseContext()));
        tabSpec2.setContent(new TabContents(getBaseContext()));
        tabSpec1.setIndicator("Product");
        tabSpec2.setIndicator("Basket");

        mTabHost.addTab(tabSpec1);
        mTabHost.addTab(tabSpec2);*/

        mTabHost.addTab(mTabHost.newTabSpec("productTab").setIndicator("Product"), ProductSectionFragment.class, null);
//        mTabHost.addTab(mTabHost.newTabSpec("productTab").setIndicator("Product"), ProductDetailsFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("basketTab").setIndicator("Basket"), BasketFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("storesTab").setIndicator("Stores"), StoresFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("myProfileTab").setIndicator("My Profile"),LoginFragment.class, null);
        mTabHost.setCurrentTab(0);
    }

   /* protected void onSaveInstanceState(Bundle outState) {
        outState.putString("tab", mTabHost.getCurrentTabTag()); //save the tab selected
        super.onSaveInstanceState(outState);
    }*/

    @Override
    public void onFragmentChange(Fragment fragmentObj, boolean addToBackTask) {
        // Create new fragment and transaction
//        Fragment newFragment = new StoreDetailsFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        // transaction.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack
        transaction.replace(android.R.id.tabcontent, fragmentObj);

//        if(fragmentObj != null && fragmentObj instanceof StoresFragment) {
//            //Fragment already exists
//        } else {
//            //Add Fragment
//        }


        if (addToBackTask) {
                transaction.addToBackStack(null);


        }
        // Commit the transaction
        transaction.commit();
    }

    public static void hideTabHost(boolean isHide){

        if(isHide){
            mTabWidget.setVisibility(View.GONE);
        }else {
            mTabWidget.setVisibility(View.VISIBLE);
        }

    }

}
