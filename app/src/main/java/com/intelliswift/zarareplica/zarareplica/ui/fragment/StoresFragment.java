package com.intelliswift.zarareplica.zarareplica.ui.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.interfaces.OnFragmentChangeListner;
import com.intelliswift.zarareplica.zarareplica.ui.adapter.StoresListAdapter;
import com.intelliswift.zarareplicacore.core.IOCContainer;
import com.intelliswift.zarareplicacore.core.IResponseSubscribe;
import com.intelliswift.zarareplicacore.core.model.stores.Store;
import com.intelliswift.zarareplicacore.core.model.stores.StoreResponse;
import com.intelliswift.zarareplicacore.core.stores.StoresFacade;

import java.util.ArrayList;

/**
 * Fragment displays stores available nearby user.
 */
public class StoresFragment extends Fragment implements AdapterView.OnItemClickListener, IResponseSubscribe, TextView.OnEditorActionListener, View.OnClickListener {

    private ProgressBar mProgressBar;
    private ListView lv_stores;
    private EditText edt_searchBox;
    private Button btn_cancel;
    private StoresListAdapter mStoresListAdpater;
    private ArrayList<Store> mStoresList;
    private String mResponse;
    private StoreResponse storeResponse;
    private Context mContext;

    private OnFragmentChangeListner mFragmentChange;

    private StoresFacade storesFacade;
    String TAG = "STORES";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_stores, container, false);

        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.tab_stores);

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar3);
        edt_searchBox = (EditText) rootView.findViewById(R.id.stores_edt_search);
        lv_stores = (ListView) rootView.findViewById(R.id.stores_lv_stores);
        btn_cancel = (Button) rootView.findViewById(R.id.stores_btn_search_cancel);

        storesFacade = (StoresFacade) IOCContainer.getInstance(getActivity()).getObject(IOCContainer.ObjectName.STORES_SERVICE, TAG);
        // Get all stores list
        storesFacade.getNearbyStores();

        mProgressBar.setVisibility(View.VISIBLE);
        mStoresList = new ArrayList<Store>();
        mStoresListAdpater = new StoresListAdapter(mContext, mStoresList);
        lv_stores.setAdapter(mStoresListAdpater);

        edt_searchBox.setOnEditorActionListener(this);
        lv_stores.setOnItemClickListener(this);
        btn_cancel.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Fragment fragment = new StoreDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("store_detail", mStoresList.get(position));
        fragment.setArguments(bundle);
        // Send the event to the host activity
        mFragmentChange.onFragmentChange(fragment, true);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        try {
            mFragmentChange = (OnFragmentChangeListner) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.my, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onResume() {
        super.onResume();
        IOCContainer.getInstance(getActivity()).publisher.registerResponseSubscribe(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Show stores on map
        if (item.getItemId() == R.id.action_menu_map) {

            StoresNearbyFragment fragment = new StoresNearbyFragment();
            Bundle bundle = new Bundle();
//            bundle.putString("stores_data", mResponse);
            bundle.putParcelable("stores_data", storeResponse);
            fragment.setArguments(bundle);
            // Send the event to the host activity
            mFragmentChange.onFragmentChange(fragment, true);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResponse(Object response, String tag) {

        if (response instanceof StoreResponse) {
            mStoresList.clear();
            storeResponse = (StoreResponse) response;
            if (storeResponse.getErrorcode().equals("0")) {
                Store mStore;
                int size = storeResponse.getStore().size();
//                mStoresList = new ArrayList<Store>();
                for (int index = 0; index < size; index++) {
                    mStore = storeResponse.getStore().get(index);
                    Log.d("Response", mStore.toString());
                    mStoresList.add(mStore);
                }
                mStoresListAdpater.notifyDataSetChanged();
                mProgressBar.setVisibility(View.GONE);
            } else {
                mProgressBar.setVisibility(View.GONE);
                // TODO : Define alertbao or show error for data response
            }
        }
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onErrorResponse(Exception error) {

    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//            performSearch();

            Toast.makeText(mContext, "Searching", Toast.LENGTH_SHORT).show();
            btn_cancel.setVisibility(View.VISIBLE);
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            storesFacade.searchStores();
            mProgressBar.setVisibility(View.VISIBLE);
            imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.stores_btn_search_cancel){
            btn_cancel.setVisibility(View.GONE);
            edt_searchBox.setText("");
            mProgressBar.setVisibility(View.VISIBLE);
            storesFacade.getNearbyStores();
        }
    }
}
