package com.intelliswift.zarareplica.zarareplica.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.android.gms.common.internal.safeparcel.a;
import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.ui.activity.ApplicationObjectsCollectionPool;
import com.intelliswift.zarareplica.zarareplica.ui.activity.MyActivity;
import com.intelliswift.zarareplica.zarareplica.ui.adapter.InstoreListAdapter;
import com.intelliswift.zarareplica.zarareplica.ui.model.BasketItem;
import com.intelliswift.zarareplicacore.core.IOCContainer;
import com.intelliswift.zarareplicacore.core.IResponseSubscribe;
import com.intelliswift.zarareplicacore.core.model.productinfo.Info;
import com.intelliswift.zarareplicacore.core.model.productinfo.InstoreAvailability;
import com.intelliswift.zarareplicacore.core.model.productinfo.ProductInfoResponse;
import com.intelliswift.zarareplicacore.core.model.productinfo.ProductsInfo;
import com.intelliswift.zarareplicacore.core.products.ProductFacade;

import java.util.ArrayList;
import java.util.List;


public class InstoreListFragment extends DialogFragment {

    private ProductFacade productFacade;
    private String TAG = "ProductDetailsFragment";
    private ProductInfoResponse productInfoResponse;
    ListView listView;
    String [] actionList = {"Medium", "Small", "Large"};
    Context ctx;
    ApplicationObjectsCollectionPool pool;
    TranslateAnimation moveLefttoRight;
    int positionClicked;
    int oldPositionClicked;
    ArrayList<BasketItem> basketItems;
    ProductsInfo mProductsInfo;
    InstoreListAdapter adapter;

    public static Fragment newInstance(int num) {
    	InstoreListFragment f = new InstoreListFragment();
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);

        return f;
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setWindowAnimations(R.style.dialog_animation);
    	View view = inflater.inflate(R.layout.fragment_instore_list, null);
        listView = (ListView) view.findViewById(R.id.lv_instore_list);
        ctx = getActivity();
        basketItems = new ArrayList<BasketItem>();
        pool = ApplicationObjectsCollectionPool.getInstance();

        mProductsInfo = (ProductsInfo)pool.get("products_info");

        List<InstoreAvailability>  instoreList = mProductsInfo.getInfo().getInstoreAvailability();
        adapter = new InstoreListAdapter(getActivity(), instoreList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                positionClicked = position;
                Button btnAccept = (Button)view.findViewById(R.id.btn_accept);

                if(btnAccept.getVisibility() == View.VISIBLE){
                    moveLefttoRight = new TranslateAnimation(0, 200, 0, 0);
                    moveLefttoRight.setDuration(1000);
                    moveLefttoRight.setFillAfter(true);
                    btnAccept.setVisibility(View.INVISIBLE);
                    btnAccept.startAnimation(moveLefttoRight);
                } else {
                    moveLefttoRight = new TranslateAnimation(200, 0, 0, 0);
                    moveLefttoRight.setDuration(1000);
                    moveLefttoRight.setFillAfter(true);
                    btnAccept.setVisibility(View.VISIBLE);
                    btnAccept.startAnimation(moveLefttoRight);
                }

                btnAccept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Toast.makeText(getActivity(),"Clicked", Toast.LENGTH_LONG).show();
                        addTobasket();

                    }
                });
            }
        });

        return view;
    }


    public void addTobasket(){

        Integer count = (Integer) pool.get("count");
        if(count != null){
            count++;

        }else {
        count = 1;
        }
        pool.put("count", count);

        BasketItem item = new BasketItem();
        item.setProdTitle(mProductsInfo.getInfo().getDetails().getTitle());
        item.setRefNum(mProductsInfo.getInfo().getDetails().getReferNo());
        item.setSize(mProductsInfo.getInfo().getInstoreAvailability().get(positionClicked).getSize());
        item.setImgUrl(mProductsInfo.getImageUrl().get(0));
        item.setItmQty("1");
        item.setPrice("500");
        item.setTotalPrice("500");

        basketItems.add(item);

        Toast.makeText(getActivity(),"Product added to basket", Toast.LENGTH_SHORT).show();

        getTargetFragment().onActivityResult(getTargetRequestCode(), 1, getActivity().getIntent());
        getDialog().dismiss();

    }
    @Override
    public void onResume() {
        super.onResume();
    }

}
