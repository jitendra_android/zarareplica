package com.intelliswift.zarareplica.zarareplica.ui.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.intelliswift.zarareplica.zarareplica.R;
import com.intelliswift.zarareplica.zarareplica.ui.fragment.BasketFragment;


public class ItemListAdapter extends BaseAdapter {

    Context context;

    public ItemListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ViewHolder viewHolder;

        if(view == null) {
            view = inflater.inflate(R.layout.basket_item_layout, null, true);
            viewHolder = new ViewHolder();
            viewHolder.itemTitle = (TextView) view.findViewById(R.id.item_title);
            viewHolder.itemRef = (TextView) view.findViewById(R.id.item_ref);
            viewHolder.itemSize = (TextView) view.findViewById(R.id.item_size);
            viewHolder.itemQty = (TextView) view.findViewById(R.id.item_qty);
            viewHolder.itemPrice = (TextView) view.findViewById(R.id.item_price);
            viewHolder.qty_num = (TextView) view.findViewById(R.id.qty_num);
            viewHolder.itemImage = (ImageView) view.findViewById(R.id.item_image);
            viewHolder.qty_dec = (ImageView) view.findViewById(R.id.qty_dec);
            viewHolder.qty_inc = (ImageView) view.findViewById(R.id.num_inc);
            viewHolder.btn_delete = (Button) view.findViewById(R.id.btn_delete);
            viewHolder.btn_cancel = (Button) view.findViewById(R.id.btn_cancel);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.qty_dec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(viewHolder.qty_num.getText().toString());
                if(qty != 0)
                    viewHolder.qty_num.setText(String.valueOf(qty-1));
            }
        });

        viewHolder.qty_inc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt( viewHolder.qty_num.getText().toString());
                viewHolder.qty_num.setText(String.valueOf(qty+1));
            }
        });

        viewHolder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Delete "+"clicked");

            }
        });

        viewHolder.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BasketFragment.itemList.closeAnimate(i);
                System.out.println("Cancel "+"clicked");
            }
        });

        return view;
    }

    class ViewHolder {
        TextView itemTitle, itemRef, itemSize, itemQty, itemPrice, qty_num;
        ImageView itemImage, qty_dec, qty_inc;
        Button btn_delete, btn_cancel;
    }
}
